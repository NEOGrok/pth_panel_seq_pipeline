#!/usr/bin/env python
# gt2num.py

from sys import stdin, stdout, argv
import numpy as np
from collections import defaultdict



def main():

    toi = argv[1:]  # tags of interest


    while True:
        #print(".")
        line = stdin.readline().strip().split()
        #print("..")
        if not line:
            break

        #print(toi)
        #print(line)
        #print("...")
        tag_dict = {t:v for t, v in [k.split('=') for k in line[5].split(";")]}
        tags = [tag_dict[x] for x in toi if x in tag_dict.keys()]
        fields = line[0:5]
            
        #tags = [x for x in line[3].split(";") if x.split('=')[0] in toi]
        #tags = [x.split('=')[1] for x in tags]
        #print(tags)
        
        #print(fields)
        #out_line = "\t".join(fields + tags)
        out_line = "\t".join(fields + tags)
        #out_line = "\t".join(tags.insert(0,fields))
        #out_line = "\t".join(map(lambda g: gts[g], line.split()))
        stdout.write("%s\n" % out_line)
        stdout.flush()


if __name__ == "__main__":
    main()

