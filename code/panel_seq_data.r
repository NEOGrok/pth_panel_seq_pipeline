message(Sys.time(), ": Loading panel seq data")

###
### Data
###



#
# ****************** File Paths ****************

# Meta files
F_META = list(
  F_GOI = "goi.tsv",
  F_TARGETS = "big_panel_targets_v2.bed",
  F_PROBES = "big_panel_probes_v2.bed",
  F_REGIONS = "REGIONS_slop300.bed",
  F_HORIZONREF = "HorizonMyeloid_vars_hg38.tsv",
  F_MAF = "MAF_format.tsv",
  F_SAMPLE_INFO = "sample_info.tsv",
  
  F_VEP_FORMAT = "vep_CSQ_format.txt",
  F_SNPEFF_FORMAT = "snpeff_ANN_format.txt"
)
F_META <- F_META %>% map(~ file.path(P_META, .))


# Variant calling result and annotation files 
F_RESULTS = list(
  #  F_REDCAP = "",
  
  # annotations
  F_COSMIC = "cosmic.tsv",
  F_DBSNP = "dbsnp.tsv",
  F_SNPEFF = "snpeff_all.tsv",
  F_VEP = "vep_all.tsv",
  
  F_CLINVAR = "clinvar.tsv",
  #F_GNOMAD = "results/",
  
  # calls
  F_CALLS = "calls.tsv"
)
F_RESULTS <- F_RESULTS %>% map(~ file.path(P_RESULTS, .))


# We want the end result to be written in MAF format.
# In the script I use shorter column names, which must at the end be mapped to the corresponding MAF column name and position.

MAF_format <- read_tsv(F_META$F_MAF, col_names = c("id", "name", "description"), col_types = "icc")

  # ###
  # ### HEADER DEFINITIONS
  # ###
{
  print("# HEADER DEFINITIONS")
  
  #
  # column names
  #
  
  # sample and variant column names
  C_CHROM <- "chrom"
  C_START <- "start"
  C_END <- "end"
  C_REF <- "ref"
  C_ALT <- "alt"
  C_VARIANT <- "variant"  # chrom_start_ref_alt
  # C_VID <- "vid"  # unique variant ID
  
  C_PATIENT <- "patient"
  C_TYPE <- "type"
  C_PANEL <- "panel"
  C_BATCH <- "batch"
  C_SAMPLE <- "sample"  # patient-[X-]panel[-batch-type], where [X-] is an optional intra-panel name for intra-panel repeated samples
  C_FILE <- "file"
  
  C_SID <- "sid"  # unique sample ID
  C_SVID <- "vid" # unique sample variant ID
  
  
  
  # annotation clumn names
  CA_GENE <- "Hugo_Symbol"
  CA_HGVSc <- "HGVSc"
  CA_HGVSp <- "HGVSp"
  
  CA_IMPACT <- "IMPACT"
  CA_SNPEFF_FEATURE <- "snpeff_feature"
  CA_SNPEFF_FEATUREID <- "snpeff_featureid"
  CA_SNPEFF_EFFECT <- "snpeff_effect"

  CA_VEP_ <- ""
  CA_VEP_ <- ""
  
  CA_DB_DBSNP <- "ID_DBSNP"
  CA_DB_COSMIC <- "ID_COSMIC"
  CA_DB_CLINVAR <- ""
  CA_DB_GNOMAD <- ""
  
  
  
  
  # type is one of
  T_PATIENT = "PATIENT"
  T_NORMAL = "NORMAL"
  
  
  
  Cs_VAR <- c(C_CHROM, C_START, C_REF, C_ALT)
  H_VCF <- str_split("chrom, start, ref, alt, DP, AD, VAF", ', ')[[1]]
  #H_SAMPLE_INFO <- str_split("sid, sample, patient, panel, type, variant, ref_VAF, filt", ', ')[[1]] # FRD, RRD, FAD, FRD
  H_SAMPLE_INFO <- str_split("sample, type, variant, ref_VAF, filt", ', ')[[1]] # FRD, RRD, FAD, FRD
  
  H_VARIANT <- str_split("chrom, start, ref, alt", ', ')[[1]]
  #H_VARIANT <- str_split("chrom, start", ', ')[[1]]
  
  
  H_ANNOT_snpEff <- str_split("snpEff_type, snpEff_impact, snpEff_start, snpEff_gene", ', ')[[1]]
  #H_ANNOT_DBs <- str_split("SYMBOL, mut, Cosmic, dbSNP_snv, dbSNP_clin, ClinVar_clin, ClinVar_noImp", ', ')[[1]] #, dnsnp, COSMIC, ClinVar"
  H_ANNOT_DBs <- str_split("SYMBOL, mut, Cosmic, dbSNP_snv, dbSNP_clin", ', ')[[1]] #, dnsnp, COSMIC, ClinVar"
  H_ANNOT_HEADER <- c(H_ANNOT_snpEff[1:2], H_ANNOT_DBs)
  
  H_CLUST <- str_split("clust, cluster", ', ')[[1]]
  
  # pre-cluster header
  H_PRE_CLUST <- c(H_VCF, H_SAMPLE_INFO, H_ANNOT_HEADER)
  
  # final header
  H_FINAL <- c(H_VCF, H_SAMPLE_INFO, H_ANNOT_HEADER, H_CLUST)
}




#
# test that necessary data is presen in P_DAT
#



  

{
  print("Loading sample info...")
  F_SAMPLE_INFO <- read_tsv(F_META$F_SAMPLE_INFO, col_names = c("sample", "panel", "batch", "type", "gender"), col_types = "ccccc") %>% unique
  print("Loading GOI...")
  D_GOI_TRANSCRIPT <- read_tsv(F_META$F_GOI, col_types = "ccc", col_names = c("SYMBOL", "chrom", "ID"))
  
  
  print("Loading Horizon Reference...")
  D_HORIZION_REF <- read_tsv(F_META$F_HORIZONREF,
                             col_names = c("SYMBOL", "mut", "chrom", "start", "ref", "alt", "V_type", "len", "COSM", "ID_COSMIC", "VAF", "hmm"), col_types = "ccciccciccnc") %>%
    replace_na(replace = list(ID_COSMIC = "", COSM = "")) %>%
    mutate(variant = str_c(chrom, start, ref, alt, sep = "_"), VAF = as.numeric(VAF)/100) %>%
    add_column(ref_var = TRUE) %>%
    select(chrom, start, ref, alt, variant, len, VAF, ref_var, SYMBOL, mut, COSM, ID_COSMIC)
  
 
  cat(Sys.time(), ": Loading COSMIC + DBSNP files...\n")
  file_paths <- file.path(c(F_RESULTS$F_COSMIC, F_RESULTS$F_DBSNP))

  D_ANNOT_DBs <- map_dfr(file_paths, function(fp)
    read_tsv(fp, skip = 1, # skip col names
             col_names = c("chrom", "start", "ref", "alt", "ID"),
             col_types = "ciccc")
  ) %>%
    mutate(variant = str_c(chrom, start, ref, alt, sep="_")) %>%
    filter(ID != ".") %>%
    unique %>%
    mutate(ID_DBSNP = ifelse(str_detect(ID, "rs"), ID, ""),
           ID_COSMIC = ifelse(str_detect(ID, "COSV"), ID, "")) %>%
    select(-ID) %>%
    group_by(variant) %>%
    mutate(ID_DBSNP = paste(ID_DBSNP, collapse = ""), ID_COSMIC = paste(ID_COSMIC, collapse = "")) %>%
    unique
  
  
  message(Sys.time(), ": Loading Clinvar files...\n")
  D_ANNOT_CLINVAR <- read_tsv(F_RESULTS$F_CLINVAR, skip = 1, col_names = c(C_CHROM, C_START, C_REF, C_ALT, "ID_CLINVAR", "clinvar"),
                              col_types = "cicccc"
  ) %>% filter(clinvar != ".") %>%
    mutate(variant = str_c(chrom, start, ref, alt, sep ="_"))
  
  
  # file_path <- file.path(P_DAT, "gnomad_exomes_gt1pct.tsv")
  # D_ANNOT_GNOMAD <- read_tsv(file_path, col_names = c(C_CHROM, C_START, "ID_DBSNP", C_REF, C_ALT, "ClinVar_AF"),
  #                            col_types = "cicccn"
  # ) %>%
  #   mutate(variant = str_c(chrom, start, ref, alt, sep ="_")) %>% unique
  # 
  
  
  # print("Loading snpEff...")
  # VCF_INFO_ANN_COLS <- read_tsv(F_META$F_SNPEFF_FORMAT, col_names = F) %>%
  #   unlist %>%
  #   str_split("\\|") %>% .[[1]]
  # D_ANNOT_SNPEFF <- read_tsv(F_RESULTS$F_SNPEFF, skip = 1,
  #                            col_types = "cciccc",
  #                            col_names = c(C_SAMPLE, C_CHROM, C_START, C_REF, C_ALT, "ANN") #c(Cs_VAR, "Hugo_Symbol", CA_SNPEFF_EFFECT, "Feature_type", "Feature", "IMPACT", "BIOTYPE", "HGVSc", "HGVSp", "LOF")
  # ) %>%
  #   mutate(sample = str_remove(sample, "_snpeff.*")) %>%
  #   separate(ANN, VCF_INFO_ANN_COLS, "\\|") %>%
  # 
  #   replace_na(replace = list(Annotation_Impact = "")) %>%
  #   mutate(variant = str_c(chrom, start, ref, alt, sep="_"),
  #          Annotation_Impact = ordered(Annotation_Impact, levels = c("", "MODIFIER","LOW","MODERATE","HIGH"))) %>%
  #   unique # %>%
  #   # 
    # dplyr::filter(Feature_type %in% c("transcript", "intergenic_region")) %>%
    # 
    # # replace NA with ""
    # replace_na(replace = list(Hugo_Symbol = "")) %>%
    # 
    # # same with the two mutation position info columns
    # replace_na(replace = list(HGVSp = "")) %>%
    # 
    # dplyr::select(Cs_VAR, C_VARIANT, Feature_type, BIOTYPE, IMPACT, Hugo_Symbol, Feature, HGVSc, HGVSp)
    # 
  
  
     print(Sys.time(), ": Loading VEP")
  VCF_INFO_CSQ_COLS <- read_tsv(F_META$F_VEP_FORMAT, col_names = F) %>%
    unlist %>%
    str_split("\\|") %>% .[[1]]
  
  D_ANNOT_VEP <- read_tsv(F_RESULTS$F_VEP, skip = 1,
                             col_types = "cciccc",
                             col_names = c(C_SAMPLE, C_CHROM, C_START, C_REF, C_ALT, "CSQ")
  ) %>%
    mutate(sample = str_remove(sample, "_vep.*")) %>%
    mutate(variant = str_c(chrom, start, ref, alt, sep="_")) %>%
    
    group_by(sample, variant) %>%
    #mutate(n_data = str_count(CSQ, "\\|")) %>% #filter(n_data == 64) %>% #.$CSQ %>% .[1] %>% str_split(., ",")
    mutate(CSQ = map(CSQ, function (x) str_split(x, ","))[[1]]) %>% unnest(CSQ) %>%
    separate(CSQ, VCF_INFO_CSQ_COLS, "\\|") %>%
    
    
    replace_na(replace = list(Allele = "", Consequence = "", BIOTYPE = "", Feature = "", IMPACT = "", Feature_type = "",
                              SYMBOL = "", EXON = "", INTRON = "", HGVSc = "", HGVSp = "", 
                              cDNA_position=0, CDS_position=0, Protein_position=0, Amino_acids=0, Codons="", Gene = 0,
                              Existing_variation="", DISTANCE = 0, STRAND = "", FLAGS="", SYMBOL_SOURCE="", HGNC_ID="", TSL="",
                              REFSEQ_MATCH="", REFSEQ_OFFSET="", GIVEN_REF="", USED_REF="", BAM_EDIT="", HGVS_OFFSET="", CLIN_SIG="",
                              SOMATIC="", PHENO="")) %>%
    
    mutate(
      IMPACT = ordered(IMPACT, levels = c("", "MODIFIER","LOW","MODERATE","HIGH")),
      BIOTYPE = ordered(BIOTYPE, levels = c("protein_coding", "lncRNA", "misc_RNA", "transcribed_pseudogene"))) %>%
    
    group_by(sample, variant) %>%
    dplyr::filter(IMPACT == max(IMPACT) | is.na(IMPACT)) %>%
    dplyr::filter(BIOTYPE == max(BIOTYPE) | is.na(BIOTYPE)) %>%
    ungroup %>%
    replace_na(replace = list(IMPACT = "")) %>%
    unique
    
 

  
  # D_ANNOT_SNPEFF %>% count(snpEff_impact)
  # D_ANNOT_SNPEFF %>%
  #   #group_by(variant) %>%
  #   slice_max(order_by = snpEff_impact, n = 5000)
  # 
  # tibble(a = sample(5, 100, replace = T)) %>% group_by(a) %>% dplyr::slice(1:3) #slice_max(order_by = a, n = 3) %>% print(n = nrow(.))
  # mtcars %>% slice_min(mpg, n = 5)
  # 
  
  message(Sys.time(), ": Loading variant calls...\n")
  D_CALLS_ALL <- read_tsv(F_RESULTS$F_CALLS, skip = 1,
                          col_types = "ccicciincc",
                          col_names = c(C_SAMPLE, C_CHROM, C_START, C_REF, C_ALT, "DP", "AD", "VAF", "extra", "caller")) %>% #select(sample) %>% unique %>% View()
    mutate(variant = str_c(chrom, start, ref, alt, sep = "_")) %>%
    mutate(sample = str_remove(sample, "_norm.*")) %>%
    
   # mutate(type = T_PATIENT)
  
    filter(!is.na(DP)) %>%  # lofreq HRUN variants have no VAF info...
    inner_join(F_SAMPLE_INFO) #select(sample) %>% unique %>% View
  
  D_CALLS_ALL %>% group_by(caller) %>% tally
  message(Sys.time(), ": Pre-filtering calls...")

  D_CALLS <- D_CALLS_ALL %>% filter(VAF != ".") %>%
    F_pre_filter2(D_HORIZION_REF, c(H_VCF, H_SAMPLE_INFO, "caller", "callers", "extra", "recurrence"))

	D_CALLS %<>% mutate(variant_len = ifelse(str_detect(alt, "<|\\.", negate = TRUE), str_length(alt) - str_length(ref), extra))


}









{
  
  message(Sys.time(), ": Joining tables...")
  
  # Joins with annotations
 var_lj <- D_CALLS %>% #slice_sample(n = 10000) %>%
    #left_join(D_ANNOT_SNPEFF) %>%
    left_join(D_ANNOT_VEP) %>%
    left_join(D_ANNOT_DBs) %>%
    left_join(D_ANNOT_CLINVAR) %>%
    #left_join(D_ANNOT_GNOMAD) %>%
    
    replace_na(replace = list(ID_DBSNP = "", ID_COSMIC = "", ID_CLINVAR = "", clinvar = "", ClinVar_AF = 0))
 
 
  
 
    
  var_lj %<>% filter(! (clinvar %in% c("Likely_benign", "Benign", "Benign/Likely_benign")) | is.na(clinvar))
}

var_lj %>% select(clinvar) %>% table


if (FALSE) {
#
# SYMBOLrate combined and consensus data
#
comb <- var_lj %>% group_by(sample, variant) %>% dplyr::slice(1) %>% ungroup
#cons <- var_lj_gene %>% group_by(Sample, variant) %>% filter(str_count(callers, ",") >= 1) %>% slice(1) %>% ungroup


comb %>% select(sample) %>% unique %>% print(n = nrow(.))
comb %>% filter(ID_DBSNP != "", ID_COSMIC != "") %>%
  group_by(callers) %>%
  mutate(n = n()) %>%
  ungroup() %>%
  select(callers, n) %>% unique %>%
  ggplot(aes(x = callers, y = n)) +
  geom_col() +
  scale_y_continuous(labels = scales::comma)

comb %>% filter(str_length(ref) > str_length(alt), ID_DBSNP != "", ID_COSMIC != "")
comb %>% filter(str_length(ref) < str_length(alt), ID_DBSNP != "", ID_COSMIC != "")
comb %>% filter(str_length(ref) < str_length(alt)) %>%
  select(H_VCF, sample, ID_DBSNP, ID_COSMIC)
comb_filt %>% filter(str_length(ref) > str_length(alt), caller == "vardict", ID_DBSNP != "", ID_COSMIC != "") %>%
  select(H_VCF, sample, ID_DBSNP, ID_COSMIC)
comb %>% filter(map2_lgl(ref, alt, function (r,a) str_length(r) == 1 & str_length(a) == 1))


comb_filt %>% filter(str_length(ref) > str_length(alt))
comb_filt %>% filter(str_length(ref) == 1 & str_length(alt) == 1)
comb_filt %>% filter(map2_lgl(ref, alt, function (r,a) str_length(r) == 1 & str_length(a) == 1))





##############################################
#### TBD ... need something out of this...
##############################################

# TBD is this right?
# TBD
# number of unique variants or number sample-variants


D_REGIONS <- read_tsv(F_META$F_REGIONS, col_names = c("chrom", "start", "end"), col_types = "cii")


genome_inner_join_closest()
dr <- D_REGIONS %>%
  genome_inner_join_closest(comb %>% mutate(end = start), by = c("chrom", "start", "end")) %>%
  mutate(reg = str_c(chrom.x, ":", start.x, "-", end.x), reg_len = end.x - start.x,
         chrom = chrom.y, start = start.y) %>%
  select(reg, reg_len, variant) %>%
  #select(reg, reg_len, chrom, start, sample, VAF, variant) %>%
  group_by(reg) %>%
  mutate(n = n(), nu = length(unique(variant)), dn = n/reg_len, dnu = nu/reg_len) %>%
  ungroup %>%
  select(-variant) %>%
  unique %>%
  mutate(dn_dnu = dn/dnu, n_nu = n/nu)


ggplot(data = dr) +
  geom_histogram(aes(x = n), fill = "blue", alpha = 0.5, bins = 400) +
  geom_histogram(aes(x = nu), fill = "red", alpha = 0.5, bins = 400) +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())

ggplot(data = dr %>% slice(1:500)) +
  geom_col(aes(x = reg, y = n), fill = "blue", alpha = 0.5, bins = 400) +
  geom_col(aes(x = reg, y = nu), fill = "red", alpha = 0.5, bins = 400) +
  theme_classic() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())

ggplot(data = dr %>% slice(1:500)) +
  geom_col(aes(x = reg, y = dn), fill = "blue", alpha = 0.5, bins = 400) +
  geom_col(aes(x = reg, y = dnu), fill = "red", alpha = 0.5, bins = 400) +
  theme_minimal() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())


ggplot(data = dr %>% slice(1:500)) +
  geom_col(aes(x = reg, y = dn_dnu), fill = "blue", alpha = 0.5, bins = 400) +
  theme_minimal() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())

# var_dens <- c()
# i <- 0
# for (r in seq_len(nrow(D_REGIONS))) {
#   i = i + 1
#   rr <- D_REGIONS[r,]
#   res <- rr %>%
#      genome_intersect(comb %>% mutate(end = start), by = c("chrom", "start", "end")) %>%
#     mutate(n = n(), nu = length(unique(vairant)), reg = str_c(rr %>% unlist, collapse = " "), reg_len = rr$end - rr$start) %>%
#     select( n, reg, reg_len) %>% unique %>%
#     mutate(var_dens_sv = round(n/reg_len, 3), var_dens_uv = round(nu/reg_len, 3)) %>%
#     select(reg, var_dens)
#   
#   var_dens <- c(unique_var_dens, ifelse(nrow(res) == 0, 0, res$var_dens))
#   
#   # if (nrow(res) > 0) {
#   #   # s <- str_c(res %>% unlist, collapse = " ")
#   #   # names(s) <- ""
#   #   # print(s)
#   # } else {
#   #   cat(rr %>% unlist, 0, "\n")
#   # }
#   #   #select(sample, chrom, start, end, VAF, n, reg, reg_len)
#   # #   relocate(c("start", "end"), .after = chrom) %>%
#   if (i %% 100 == 0) print(i)
# }
# 
# i
# sample_var_dens <- var_dens
# unique_var_dens
# 
# p1 <- sample_var_dens %>% enframe() %>%
#   filter(value > 0.01) %>%
#   ggplot(aes(x = value)) +
#   geom_histogram(bins = 100) +
#   theme_minimal()
# 
# p2 <- unique_var_dens %>% enframe() %>%
#   filter(value > 0.01) %>%
#   ggplot(aes(x = value)) +
#   geom_histogram(bins = 100) +
#   theme_minimal()
# 
# 
# p1 / p2
# 
# 
# hist(var_dens, breaks = 100)
# 
# 
# 
# 
# ptm <- proc.time()
# comb %>% filter(chrom == rr$chrom & start >= rr$sample & end <= rr$end)
# proc.time() - ptm
# 
# ptm <- proc.time()
# rr %>% genome_intersect(comb %>% mutate(end = start), by = c("chrom", "start", "end"))
# proc.time() - ptm
# 
}