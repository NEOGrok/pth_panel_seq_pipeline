
message("Running panel seq filtering pipeline")



# *************************
# *** TESTS/CONSTRAINTS ***
# *************************

# *** VAR PREP TESTS ***
# TEST: each sample variant appears at most 1x
#(comb %>% select(sample, variant) %>% nrow) == (comb %>% select(sample, variant) %>% unique %>% nrow)

# *** FILTERING TESTS ***
# TEST: captures true variants
# TEST: removes "bad" variants

# *************************


# comb_FCF %>% filter(start == 28034079) %>% View
# comb_FCF %>% filter(start == 28034084) %>% View
# comb_FCF %>% filter(start == 5073770) %>% select(sample, chrom, start, ref, alt, SYMBOL, DP, AD, VAF)


comb_FCF <- comb %>% filt_clust_filt
comb_filt <- comb_FCF %>% dplyr::filter(filt == "")


#comb_FCF %>% filter(alt == "<DUP>") %>% select(H_VCF, extra, caller, filt) %>% print(n = nrow(.))
#comb_filt %>% filter(alt == "<DUP>" & extra < 500) %>% select(H_VCF, extra, caller) %>%
#  mutate(ALT = system(
#    str_c("samtools faidx", REF_GEN, str_c(as.character(chrom),":",as.character(start),"-", as.character(start+extra-1)), "| grep -v ">" | tr -d '\n'", sep = " ")))


# cons_FCF <- cons %>% filt_clust_filt
# cons_filt <- cons_FCF %>% filter(filt == "")
# 
# 
# comb_FCF %>% filter(start == "28063699") %>% View
# comb_FCF %>% filter(start == "5073770") %>% View
# comb_FCF %>% filter(start == "28033909") %>% View
# comb_FCF %>% filter(start == "28033910") %>% View


# comb_filt %>% select(variant, recurrence, SYMBOL) %>% arrange(desc(recurrence)) %>% unique
# comb_filt %>% group_by(SYMBOL) %>% mutate(recurrence_gene = length(sample)) %>% arrange(desc(recurrence_gene)) %>% select(SYMBOL, recurrence_gene) %>% unique
# comb_filt %>% group_by(SYMBOL) %>% mutate(recurrence_gene = length(unique(sample))) %>% arrange(desc(recurrence_gene)) %>% select(SYMBOL, recurrence_gene) %>% unique





