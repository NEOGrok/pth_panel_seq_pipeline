# get options, using the spec as defined by the enclosed list.
# we read the options from the default: commandArgs(TRUE).
args = commandArgs(trailingOnly = TRUE)

sample_name <- args[1L]
out_dir <- args[2L]
sample_pos_cig_seq_path <- args[3L]   # tsv with sample pos cigar sequence
min_cigar_spike <- args[4L]           # min cigar spike size for it to be considered a potential ITD


message(   
  sprintf("
          arg1 = %s\n
          arg2 = %s\n
          arg3 = %s\n
          arg4 = %s\n", args[1L], args[2L], args[3L], args[4L])
  )


spec = matrix(c(
  'verbose', 'v', 2, "logical",
  'help'   , 'h', 0, "logical",
  'main'  , 'd', 1, "character",
  'run_tests', 't', 2, "logical"
), byrow=TRUE, ncol=4)
opt = getopt(spec)


# if help was asked for print a friendly message
# and exit with a non-zero error code
if ( !is.null(opt$help) ) {
  cat(getopt(spec, usage=TRUE))
  q(status=1)
}

# set some reasonable defaults for the options that are needed,
# but were not specified.
if ( is.null(opt$main   ) ) { opt$main    = "dat"    }
if ( is.null(opt$verbose ) ) { opt$verbose = FALSE }


# do some operation based on user input.
P_MAIN = opt$main
V_VERBOSE = opt$verbose


# args = commandArgs(trailingOnly = TRUE)
# message(sprintf("Hello %s", args[1L]))
