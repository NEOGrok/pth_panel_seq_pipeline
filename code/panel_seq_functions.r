message(Sys.time(), ": Loading panel seq functions")

#
# each caller has its own algo => might call different variants + different ways of resolving what looks like an indel
#   => there can be multiple variant calls per (chrom,start) locus
#   to get around this we make use of multiple callers 
#   if there r multiple variants for a given locus
#     pick the one called by >= 2 callers
#   if the locus is calledf by only one caller, we pick it if
#     - DP > 200, VAF >2%
#     if there r >1 variants for that locus, we pick the one with highest DP, then VAF (then lowest strand bias)
#
F_pre_filter2 <- function(df, d_horizon_ref, h_header) {
  # there are sometimes multiple variant calls per sample per location
  # pick the sample variant location with max VAF and DP
  df %>%
    unique %>%
    
    group_by(variant) %>%
    
    # recurrence = how many different samples have this variant?
    # (Recurrent SYMBOLs/calls)
    mutate(recurrence = length(unique(sample))) %>%
    
    ungroup %>%
    
    group_by(sample, variant) %>%  # only ONE sample-variant must remain

    # which callers called the variant?
    mutate(callers = str_c(unique(caller), collapse = ",")) %>%
    
    # if > 1 call per sample variant, keep only the one w the highest VAF & DP
    # TBD other ways of doing this: take min or take avg VAF & DP
    mutate(max_VAF = max(VAF)) %>% dplyr::filter(VAF == max_VAF) %>%
    mutate(max_DP = max(DP)) %>% dplyr::filter(DP == max_DP) %>%
    dplyr::slice(1) %>%  # pick one if >1 with the same VAF and DP
   
    #mutate(shortest_ref = min(map_int(ref, function(r) str_length(r)))) %>% filter(str_length(ref) == shortest_ref) %>%
    #mutate(shortest_alt = min(map_int(alt, function(r) str_length(r)))) %>% filter(str_length(alt) == shortest_alt) %>% #filter(chrom == "1", start == 43824405) %>% select(sample, variant, type, ref, alt, DP, VAF, dbSNP_snv) %>% arrange(sample) %>% print(n = nrow(.))#tally %>% filter(n > 1)
    
    #filter(snpEff_impact == max(snpEff_impact)) %>%
    #slice(1) %>%
    
    #filter(length(VAF) == 1) %>%  # from those variants that are left, just removes those that still have multiple calls per sample
    ungroup %>% unique %>%
    
    
    # add reference var VAF to each variant that's amongst the ref vars
    mutate(ref_VAF = map_dbl(variant, function(v) ifelse(v %in% d_horizon_ref$variant, d_horizon_ref$VAF[which(d_horizon_ref$variant == v)], 0))) %>%
    
    # this field will contain the filters applied during the variant filtering process
    mutate(filt = "") %>%
    
    dplyr::select(h_header)
}





#
# Error Handling
# --------------

# run this as: str(tryCatch.W.E( my_function(x,y) ))
tryCatch.W.E <- function(expr) {
  W <- NULL
  w.handler <- function(w){ # warning handler
    W <<- w
    invokeRestart("muffleWarning")
  }
  list(value = withCallingHandlers(tryCatch(expr, error = function(e) e),
                                   warning = w.handler),
       warning = W)
}


ctypes <- function(df) {
  str_c(
    sapply(df, function(x) str_sub(class(x), end = 1)),
    collapse = "")
}



#
# Filter Definitions
# ------------------



ifelse(c(F,T,F), c("a","aa"), c("b","","c"))

# A filter always adds (to) feature: filter = NA, 'F001', 'F002', 'F003', ...
# A filter can add extra features, but doesn't change any existing features
update_filt <- function(cond, filt, fID) {
  # if cond: add fID, else: keep as is
  ifelse(cond, str_c(filt, fID, sep = ":"), filt)
}

F000 <- function(df, min_DP) {
  # Description: filter if DP < min_DP
  fID = "F000"
  cat("Filter ", fID, "\n")
  
  df <- comb %>% 
    mutate(filt = update_filt(DP < min_DP, filt, fID))
  print(df %>% count(filt))
  
  df
}

F001 <- function(df) {
  # Description: filter variants with VAF >= 0.2 present in at least 1 NORMAL
  fID = "F001"  # filter ID
  cat("Filter ", fID, "\n")
  
  rm_var <- df %>% #filter(chrom == "X", start == 129190010) %>%
    filter(type == T_NORMAL & VAF >= 0.2) %>%
    select(variant) %>% unique %>% as_vector
  df %<>% 
    mutate(filt = update_filt(variant %in% rm_var, filt, fID)) #%>% filter(chrom == "X", start == 129190010)
  
  print(df %>% count(filt))
  df
}


F002 <- function(df) {
  # Description: filter variants present in at least 2 NORMALS with 0.1 <= VAF < 0.2
  fID = "F002"
  cat("Filter ", fID, "\n")
  
  rm_var <- df %>%
    group_by(variant) %>%
    mutate(f = (type == T_NORMAL & (VAF >= 0.1 & VAF < 0.2))) %>%
    filter(f) %>% tally %>% filter(n >= 2) %>% ungroup %>%
    
    select(variant) %>% unique %>% as_vector
  
  
  df %<>% 
    mutate(filt = update_filt(variant %in% rm_var, filt, fID))
  print(df %>% count(filt))
  df
}


F003 <- function(df) {
  # Description: keep only variants with a MODERATE or HIGH or MODIFIER VEP impact level and those that do not have a SnpEff annotion
  # Comments: FLT3ITD had a MODIFIER IMPACT annotation => need to keep that as well.
  fID = "F003"
  cat("Filter ", fID, "\n")
  
  df %<>% 
    mutate(filt = update_filt(!(IMPACT %in% c("MODERATE", "HIGH", "MODIFIER", "")), filt, fID)) # %>% filter(filt == "")
  print(df %>% count(filt))
  df
}

F004 <- function(df) {
  # Description: filter NORMAL only variants, i.e. if no PATIENT sample has the variant
  fID = "F004"
  cat("Filter ", fID, "\n")
  
  df %<>% 
    group_by(variant) %>%
    mutate(no_patients = (sum(type == T_PATIENT) == 0)) %>%
    ungroup %>%
    mutate(filt = update_filt(no_patients, filt, fID)) %>%
    select(-no_patients) #%>%
  #dplyr::filter(filt == "")
  print(df %>% count(filt))
  df
}



F_clust_mark <- function(df) {
  # Description
  # Mark variants for clustering:
  #   variants present in at least 1 of each type (NORMAL and PATIENT)
  #   variants present in N >= 40 PATIENTs
  cat("Marking variants for clustering.", "\n")
  
  df %>%
    group_by(variant) %>%
    mutate(clust = ifelse(length(unique(type)) == 2 | length(type == T_PATIENT) >= 40, TRUE, FALSE)) %>%
    ungroup
}


F005 <- function(df) {
  # Description: filter clusters containing at least 1 NORMAL
  fID = "FCN"
  cat("Filter ", fID, "\n")
  
  df %<>%
    group_by(variant, cluster) %>%
    mutate(n_normal = sum(type == T_NORMAL),
           filt = update_filt(sum(type == T_NORMAL) > 1, filt, str_c(fID, n_normal, sep = ""))) %>%
    ungroup
  
  print(df %>% count(filt))
  df
}

F006 <- function(df, N_sd = 4) {
  # Description:
  # filter clustered variants where
  #   min(upper cl) > N_sd x max(sd_cl1, sd_cl2) + max(lower cl) && min_ucl < 0.05 + max_lcl
  #     note: "upper cl" = cl1 if mean(cl1) > mean(cl2), else cl2
  #   or 
  fID = str_c("F", N_sd, "SD", sep = "")
  cat("Filter ", fID, "\n")
  
  df %<>%
    # filtering is per row
    # all info needs to be present in every row
    
    # add meanm, sd, min, max for each cluster
    group_by(variant, cluster) %>%
    mutate(sd_cl = ifelse(length(VAF) > 1, sd(VAF), 0)) %>%
    mutate(mean_cl = mean(VAF)) %>%
    mutate(max_cl = max(VAF)) %>%
    mutate(min_cl = min(VAF)) %>%
    ungroup %>%
    
    # for each variant: add sd, mean, min, max for both clusters
    group_by(variant) %>%
    mutate(sd_cl1 = ifelse(cluster == 1, sd_cl, 0)) %>%
    mutate(sd_cl2 = ifelse(cluster == 2, sd_cl, 0)) %>%
    mutate(sd_cl1 = ifelse(cluster == 2, max(sd_cl1), max(sd_cl1))) %>%
    mutate(sd_cl2 = ifelse(cluster == 1, max(sd_cl2), max(sd_cl2))) %>%
    
    mutate(mean_cl1 = ifelse(cluster == 1, mean_cl, 0)) %>%
    mutate(mean_cl2 = ifelse(cluster == 2, mean_cl, 0)) %>%
    mutate(mean_cl1 = ifelse(cluster == 2, max(mean_cl1), max(mean_cl1))) %>%
    mutate(mean_cl2 = ifelse(cluster == 1, max(mean_cl2), max(mean_cl2))) %>%
    
    # add upper_cluster boolean, NA if cluster = 0 (unclustered)
    mutate(upper_cl = (mean_cl == max(mean_cl1, mean_cl2)) ) %>%
    ungroup %>%
    
    group_by(variant, cluster) %>%
    mutate(min_uc = ifelse(upper_cl, min(VAF), 0)) %>%
    mutate(max_lc = ifelse(!upper_cl, max(VAF), 0)) %>%
    ungroup %>%
    
    group_by(variant) %>%
    mutate(min_uc = ifelse(!upper_cl, max(min_uc), min_uc)) %>%
    mutate(max_lc = ifelse(upper_cl, max(max_lc), max_lc)) %>%
    
    
    # add filter boolean to all clustered variants... keep all unclustered variants
    # filter if min_uc < max_lc + N_sd x max(sd_cl1, sd_cl2)
    mutate(n_sd = round((min_uc - max_lc)/max(sd_cl1,sd_cl2),2),  # how many SD is min_uc from max_lc?
           filt = update_filt( clust & ifelse(max(sd_cl1, sd_cl2) < 0.005,   # if it's clustered and max sd is (almost) 0
                                              min_uc < (0.05 + max_lc),      # filter if they're within 0.05 of each other
                                              n_sd < N_sd & min_uc < (0.05 + max_lc)),  # else filter if 
                               filt, str_c(fID, n_sd, sep = "")
           )) %>%
    
    ungroup
  
  print(df %>% count(filt))
  df
}


F007 <- function(df) {
  # Description: filter PATIENT variants with a VAF < 0.02
  fID <- "FV<2"
  cat("Filter ", fID, "\n")
  
  df %<>%
    mutate(filt = update_filt(type == T_PATIENT & VAF < 0.02, filt, fID))
  
  print(df %>% count(filt))
  df
}


F008 <- function(df, N) {
  # Description: filter clusters with >= N PATIENTs with a VAF > 0.02
  fID <- str_c("F#P>", N, collapse="")
  cat("Filter ", fID, "\n")
  
  df %<>%
    group_by(variant, cluster) %>%
   # mutate(k = sum(type == T_PATIENT & VAF > 0.02) >= N) %>%
    mutate(filt = ifelse(sum(type == T_PATIENT & VAF > 0.02) >= N, str_c(filt, fID, sep = ":"), filt)) %>%
    #mutate(filt = update_filt(sum(type == T_PATIENT & VAF > 0.02) >= N, , fID)) %>%
  #  mutate(filt = map_chr(filt, ~update_filt(sum(type == T_PATIENT & VAF > 0.02) >= N, ., fID))) %>%
    ungroup
  
  print(df %>% count(filt))
  df
}


F009 <- function(df, whitelist) {
  # Description: keep all the variants on the whitelist, even if filtered by previous filters, i.e. empty their filt feature.
  fID <- "WHITELIST"
  cat("Filter ", fID, "\n")
  
  for (rn in seq_len(nrow(whitelist))) {
    if (sum(df$variant == whitelist[rn,]$variant) > 0) {
        df[df$variant == whitelist[rn,]$variant,]$filt <- ""
    }
  }
  
  print(df %>% count(filt))
  df
}



cluster_h <- function(df) {
  cat("Clustering...", "\n")
  
  df_0 <- df %>% filter(!clust) %>% mutate(cluster = factor(1, levels = 1:2))
  
  df_1 <- df %>%
    filter(clust) %>% #filter(chrom == "19", start == 33792731)
    group_by(chrom, start, ref, alt) %>%
    nest %>% #print(n = nrow(.))
    
    # hierarchical clustering (hc)
    mutate(hc = map(data, function(cd) hclust(dist(cd %>% select(VAF, sample) %>% column_to_rownames(var = "sample" )))))  %>%
    mutate(data2 = map2(data, hc,
                        function(df, hc_i)
                          inner_join(df, by = "sample",
                                     #data.frame(clusters = factor(cutree(hc_i, h=0.1))) %>%
                                     data.frame(cluster = factor(cutree(hc_i, k=2), levels = 1:2)) %>%
                                       rownames_to_column(var = "sample")))
    ) %>%
    ungroup %>%
    select(-c(data, hc)) %>%
    unnest(data2)
  
  bind_rows(df_0, df_1)
}


#df <- comb
#df <- vls

#vls %>% group_by(sample, chrom, start) %>% slice(1) %>% tally
filt_clust_filt <- function(df) {
  comb %>% F000(min_DP = 5) %>% F001 %>% F002 %>% F003 %>% F004 %>%
    F_clust_mark %>% cluster_h %>%
    F005 %>% F006(N_sd = 4) %>% F007 %>% F008(14) %>% #%>% filter(chrom == 13, start == 28033910) %>% view
		F009(variant_whitelist)
}

# comb %>%  filter(start == 32434638) %>% F000(min_DP = 30) %>% F001 %>% F002 %>% F003 %>% F004 %>%
#   F_clust_mark %>% cluster_h %>%
#   F005 %>% F006(N_sd = 4) %>% View F007 %>% F008(16) %>% View









# ###
# ### MISC
# ###

count_ref_vars <- function(df, d_ref) {
  #df %>% select(variant, ref_var) %>% unique %>% count(ref_var)
  d_ref %>% filter(variant %in% df$variant) %>% count
}
find_ref_vars <- function(df, d_ref) {
  d_ref %>% filter(variant %in% df$variant) %>% select(variant, VAF)
}
find_missing_ref_vars <- function(df, d_ref) {
  d_ref %>% filter(!(variant %in% df$variant)) %>% select(variant, VAF)
}





# ###
# ### PLOTTING FUNCTIONS
# ###
# ==================
# comb %>%
#   mutate(ref_type = str_c(str_detect(sample, "Horizon"), type, sep = "_")) %>%
#   filter(variant %in% D_HORIZION$variant, start == 32434638) %>% View() select(ref_type) %>% print(n=nrow(.))

# plot Horizon Myeloid Vars
plot_reference <- function(df, d_horizon_ref, plot_name) {
  ref_plot <- d_horizon_ref %>%
    
    ggplot(aes(x = as.factor(variant), y = VAF), size = 2) +
    geom_point_interactive(
      aes(tooltip = str_c(SYMBOL, str_c(ref, alt, sep = " : "), COSM, str_c("VAF=",VAF), sep = "\n"),
          data_id = str_c(SYMBOL, COSM)),
      color = "blue", shape = "square", size = 3) +
    
    geom_jitter_interactive(data = df %>%
                              mutate(ref_type = str_detect(sample, "Horizon")) %>%
                              filter(variant %in% d_horizon_ref$variant),
                            aes(x = as.factor(variant), y = VAF, color = ref_type, shape = type,
                                tooltip = str_c(
                                  sample, type, variant, extra, IMPACT, SYMBOL, str_c("VAF=",VAF),
                                  sep = "\n"),
                                data_id = sample),
                            alpha = 0.8, size = 1.5, width = 0.3, show.legend = T) + 
    #geom_point(color = "red", shape = "square", size = 3) +
    #geom_point(data = df %>% mutate(var_start = str_c(chrom, start, sep = "_")) %>% filter(var_start %in% mutate(horizon_myel_vars, var_start = str_c(chrom, start, sep = "_"))$var_start),
    #           aes(x=as.factor(var_start), y = VAF), color = "blue", size = 2, alpha = 0.8) +
    scale_color_manual(values=c("grey", "red")) +
    scale_shape_manual(values = c(15, 16)) +
    theme_minimal() +
    theme(
      axis.text.x = element_text(angle = 90, vjust = 0.5, size = 7),
      plot.margin = unit(rep(0.5, 4), "cm")
    ) +
    labs(x="") +
    ggtitle("Horizon Myeloid reference Variants")
  
  pdf(str_c(plot_name,".pdf"))
  print(ref_plot)
  dev.off()
  
  str( tryCatch.W.E(
    htmlwidgets::saveWidget(file = str_c(plot_name, ".html", sep = "_"), girafe(print(ref_plot)))
  ))
  
  girafe(ggobj = ref_plot)
}





plot_stats <- function(df, df_filt, pdf_name) {
  p1 <- df %>% ggplot(aes(x = DP)) +
    geom_histogram(bins = 200) +
    geom_vline(xintercept = 201, color = "red") +
    geom_text(aes(x=DP, y=300, label = ifelse(DP == 200, "200", "")), color = "violet", alpha = 0.8) +
    geom_histogram(data = df_filt, aes(x = DP), fill = "grey", alpha = 0.7, bins = 100)
  
  p2 <- df %>% ggplot(aes(x = VAF)) +
    geom_histogram(bins = 200) +
    geom_histogram(data = df_filt, aes(x = VAF), fill = "grey", alpha = 0.7, bins = 100)
  
  pp <- p1 + p2 + plot_layout(nrow = 2)   # requires library(patchwork)
  
  pdf(pdf_name)
  print(pp)
  dev.off()
  
  print(pp)
}









# Visualization
#
# - red = VARIANT
# - black = FUILTERED AWAY
#
# x-axis
#   - label: chrom start - SYMBOL name
#   - label_color
#     - brown = SYMBOL of interest
#     - red = variant of interest in SYMBOL of interest
#


#plot_variants_chrom(comb_filt, D_HORIZION_REF, "3", D_GOI_TRANSCRIPT$SYMBOL)

# df <- comb_filt
# d_horizon_ref <- D_HORIZION_REF
# chromosome <- "3"
plot_variants_chr <- function(df, d_horizon_ref, chromosome, GOI, TITLE = "") {
  # df = variant data
  # d_horizon_ref = Horizon ref data
  # chromosome = which chromosome to plot
  # GOI = vector SYMBOL names to highlight (in red)
  
  df <- df %>% unique %>%
    filter(chrom == chromosome) %>%
    arrange(chrom, start) %>%
    #mutate(is_variant = str_c(ifelse(filt == "", "VARIANT", "FILTERED"), cluster, sep = "_")),
    mutate(is_variant = ifelse(filt == "", "VARIANT", "FILTERED")) %>%  # 
    mutate(start = ordered(start)) %>%
    mutate(gene_start = str_c(start, SYMBOL, sep = " - ")) %>%
    mutate(gene_start = fct_reorder(gene_start, start, min)) %>%
    mutate(cluster_type = str_c(cluster, type, sep = "_"))
  
  hmv <- d_horizon_ref %>%
    filter(chrom == chromosome) %>%
    filter(variant %in% df$variant) %>%
    mutate(start = ordered(start)) %>%
    mutate(gene_start = str_c(start, SYMBOL, sep = " - ")) %>%
    mutate(gene_start = fct_reorder(gene_start, start, min)) 
  
  
  
  gene_start <- unique(df$gene_start)
  SYMBOLs <- df %>% dplyr::select(start, SYMBOL) %>% unique %>% dplyr::select(SYMBOL) %>% unlist
  length(SYMBOLs)
  color_GOI <- ifelse(SYMBOLs %in% GOI, "brown", "black")
  names(color_GOI) <- SYMBOLs
  color_GOI <- factor(color_GOI[order(gene_start)], ordered = TRUE)
  
  # check header signature
  # TBD ...
  
  # plot
  p_dot <- df %>%
    # x-axis labels: genomic startition - SYMBOL name
    #mutate(gene_start = factor(.$start, levels = unique(.$start))) %>%
    #mutate(gene_start = str_c(start, SYMBOL, sep = " - ")) %>%
    
    ggplot(aes(x=gene_start, y=VAF, color = is_variant, shape = type, color = filter))
  
  if (dim(hmv)[1] > 0) {
    p_dot <- p_dot +
      geom_point_interactive(data = hmv,
                             # geom_point_interactive(data = horizon_myel_vars %>% filter(chrom %in% chromosomes),
                             aes(x = gene_start, y = VAF,
                                 tooltip = str_c(SYMBOL, COSM, str_c("VAF=",VAF), sep = "\n"), data_id = str_c(SYMBOL, COSM)), color = "brown", shape = 7, size = 2, alpha = 0.7)
  }
  
  p_dot <- p_dot +
    geom_jitter_interactive(size = 1, width = 0.2, height = 0,
                            aes(tooltip = str_c(sample, variant, type, extra, str_c("VAF=",round(VAF,2)), SYMBOL, ID_DBSNP, ID_COSMIC, clinvar, filt, sep = "\n"), data_id = sample )) +  #Cosmic, dbSNP_snv, 
    scale_color_manual(values=c(FILTERED = "grey", VARIANT = "red")) +
    #scale_color_manual(values=c(FILTERED_1 = "black", FILTERED_2 = "#777777", VARIANT_1 = "red", VARIANT_2 = "pink")) +
    
    theme_minimal() +
    #facet_wrap(~chrom, scales = "free_x", ncol = 1, nrow = 2) +
    theme(
      axis.text.x = element_text(angle = 90, vjust = 0.5, size = 4, color = color_GOI),
      plot.margin = unit(rep(0.5, 4), "cm")
    ) + ylim(c(0,1)) + ggtitle(str_c(chromosome, TITLE, sep = ":"))
  
  
  return(p_dot)
}





print_variant_plots <- function(pp, pdf_name) {
  
  N_pages <- n_pages(p + facet_wrap_paginate( ~chrom, scales = "free_x", ncol = 1, nrow = 1, page = 1))
  
  pdf(pdf_name)
  for (pg in seq_len(N_pages)) {
    print(pg)
    # print needed else it doesn't hv time to plot to pdf... TBD
    print(df_dot + facet_wrap_paginate( ~chrom, scales = "free_x", nrow = 1, ncol = 1, page = pg))
  }
  dev.off()
  
}
