#!/usr/bin/env bash

# Description:
# The normalized samples *_norm.vcf.gz are annotated with
# - snpEff
# - VEP
# - dbsnp
# - cosmic
# - clinvar
#

function concat_files () {
    echo "Concatenating files"
	local OUT_FILE=$1
    shift
    local IN_FILES=($@)

	for f in ${IN_FILES[@]}; do
	sed -n '1!p' $f
	done | cat <(sed -n '1p' ${IN_FILES[1]}) - > $OUT_FILE
}



add_caller () {
	# add caller column to input file
	local CALLER=$1
	local FILE=$2
    echo "Adding $CALLER column"
    awk -v OFS="\t" -v caller=$CALLER 'NR==1 {print $0, "CALLER"} NR>1{print $0, caller}' $FILE | sponge $FILE
}


function extract_annot () {
    echo "Filtering Annotations";
    
    local SIFT_FILTER="$1" # a string
    local EXTENSION=$2
    local IN_DIR="$3"
    local OUT_FILE="$4"
    local CALLER=${5:-""}

    local FILES=($IN_DIR/*_${EXTENSION}.vcf.gz) # an array
    $DEBUG && echo "${FILES[@]}"
    
    #echo ${FILES[@]}
    #echo $SIFT_FILTER

    local TMP_DIR=$IN_DIR/tmp

    if [ -d $TMP_DIR ]; then
        echo "$TMP_DIR exists... removing"
        rm -r $TMP_DIR
    fi
    
    mkdir $TMP_DIR


    for i in $(seq ${#FILES[@]}); do
    #for i in $(seq 3); do
        f=${FILES[$i-1]}
        echo "$i/${#FILES[@]}: filtering $f"
        local out_file=${TMP_DIR}/$(basename ${f%.vcf.gz}_filt.tsv)

        zcat $f | ${SNPEFF_SCRIPTS}/vcfEffOnePerLine.pl |\
        SnpSift extractFields -e "." - $SIFT_FILTER |\
        awk -v s=$(basename ${f%%_*}) -v OFS="\t" '{if (NR==1) print "SAMPLE", $0; else print s,$0}' > $out_file
    done
    
    concat_files $OUT_FILE "${TMP_DIR}/*_${EXTENSION}_filt.tsv"
    #sort ${OUT_DIR}/*_${DB}_filt.tsv | uniq > $OUT_FILE
    if [[ $CALLER != "" ]]; then add_caller $CALLER $OUT_FILE; fi

    rm -r $TMP_DIR
}



function main() {
. ./definitions.sh


# get FLT3 ITD
echo
echo "Extracting softclip spikes"
zgrep -P "TYPE=(Insertion|DUP)" $RESULTS/vardict/norm/*_norm.vcf.gz | grep -P ":13\t" | while read s r; do echo -e "$(basename $s)\t$r"; done | sed 's/:/      /' | awk '{split($1,a,"_    "); print a[1], $2,$3,$5,$6, $9}' | ../code/get_vardict_tag.py DP VD AF SVLEN | awk -v OFS="\t" '$3 > 28033800 && $3 < 28034200 && $9 > 5 {print $0, "vardict"}' | sort -k2,2n > $RESULTS/vardict/PTH_FLT3_vardict.tsv
#grep -v "^#" $RESULTS/pindel/raw/*_SI.vcf | grep -P ":13\t" | while read s r; do echo -e "$(basename $s)\t$r"; done | sed 's/:/      /' | awk '{split($1,a,"_"); print a[1],$2,$3,$5,$6,$9,$11}' | ../code/get_pindel_tag.py SVLEN | awk -v OFS="\t" '$3 > 28033800 && $3 < 28034200 && $9 > 5 {print $0, "pindel"}'
grep -v "^#" $RESULTS/pindel/*_SI.vcf | grep -P ":13\t" | while read s r; do echo -e "$(basename $s)\t$r"; done | sed 's/:/      /' | awk '{split($1,a,"_"); print a[1],$2,$3,$5,$6,$9,$11}' | ../code/get_pindel_tag.py SVLEN | awk -v OFS="\t" '$3 > 28033800 && $3 < 28034200 && $9 > 5 {print $0, "pindel"}' | sort -k3,3n > $RESULTS/pindel/PTH_FLT3_pindel.tsv
cat $(ls -A $RESULTS/softclip/*.tsv | grep -v "_spcs") | grep -v "^sample" | awk -v OFS="\t" '{print $1,$2,$3,$4,$5,$6,$7,$8,$9,"softclip"}' > $RESULTS/softclip/PTH_FLT3_softclip.tsv
cat $RESULTS/vardict/PTH_FLT3_vardict.tsv $RESULTS/softclip/PTH_FLT3_softclip.tsv $RESULTS/pindel/PTH_FLT3_pindel.tsv | sort -k2,2n > $RESULTS/PTH_FLT3.tsv



# get calls
SNPSIFT_FILTER_VARDICT="CHROM POS REF ALT GEN[0].DP GEN[0].VD AF SVLEN"
CALLS_VARDICT="$RESULTS/vardict/vardict.tsv"
extract_annot "$SNPSIFT_FILTER_VARDICT" "norm" "$RESULTS/vardict/norm" "$CALLS_VARDICT" "vardict"

SNPSIFT_FILTER_LOFREQ="CHROM POS REF ALT DP AF HRUN"   # HRUN has the the Homopolymer length
CALLS_LOFREQ="$RESULTS/lofreq/lofreq.tsv"
extract_annot "$SNPSIFT_FILTER_LOFREQ" "norm" "$RESULTS/lofreq/norm" "$CALLS_LOFREQ" "lofreq"
awk -v OFS="\t" 'NR>1{print $1,$2,$3,$4,$5,$6,int($6*$7),$7,$8,$9}' "$CALLS_LOFREQ" | sponge $CALLS_LOFREQ

SNPSIFT_FILTER_SNVER="CHROM POS REF ALT DP AC"
CALLS_SNVER="$RESULTS/snver/snver.tsv"
extract_annot "$SNPSIFT_FILTER_SNVER" "norm" "$RESULTS/snver/norm" "$CALLS_SNVER" "snver"
awk -v OFS="\t" 'NR>1{print $1,$2,$3,$4,$5,$6,$7,$7/$6,".",$8}' "$CALLS_SNVER" | sponge $CALLS_SNVER

cat "$CALLS_VARDICT" "$CALLS_LOFREQ" <(cat "$CALLS_SNVER" | sed -E 's/^([A-Za-z0-9-]+)[A-Za-z._]+\b(.*)/\1\2/') $RESULTS/PTH_FLT3.tsv | uniq > "$RESULTS/calls.tsv"  # sed removes .filter and .indel.filter added by snver to the file names



# get VEP 
echo
echo "Extracting VEP"
SNPSIFT_FILTER_VEP="CHROM POS REF ALT CSQ"
extract_annot "$SNPSIFT_FILTER_VEP" "vep" "$RESULTS/vardict/vep" "$RESULTS/vardict/vardict_vep.tsv"
extract_annot "$SNPSIFT_FILTER_VEP" "vep" "$RESULTS/lofreq/vep" "$RESULTS/lofreq/lofreq_vep.tsv"
extract_annot "$SNPSIFT_FILTER_VEP" "vep" "$RESULTS/snver/vep" "$RESULTS/snver/snver_vep.tsv"
#extract_annot "$SNPSIFT_FILTER_VEP" "vep" "$RESULTS/hc" "$RESULTS/hc/hc_vep.tsv"

cat "$RESULTS/vardict/vardict_vep.tsv" "$RESULTS/lofreq/lofreq_vep.tsv" "$RESULTS/snver/snver_vep.tsv" > "$RESULTS/vep_all.tsv"
cat "$RESULTS/vardict/vardict_vep.tsv" "$RESULTS/lofreq/lofreq_vep.tsv" "$RESULTS/snver/snver_vep.tsv" | cut -f2- | sort | uniq > "$RESULTS/vep.tsv"
cat <(grep "^SAMPLE" "$RESULTS/vep_all.tsv" | head -1) <(grep -v "^SAMPLE" "$RESULTS/vep_all.tsv") | sponge "$RESULTS/vep_all.tsv"
cat <(grep "^CHROM" "$RESULTS/vep.tsv" | head -1) <(grep -v "^CHROM" "$RESULTS/vep.tsv") | sponge "$RESULTS/vep.tsv"



# get snpEff
echo
echo "Extracting snpEff"
#SNPSIFT_FILTER_SNPEFF="CHROM POS REF ALT ANN[*].GENE ANN[*].EFFECT ANN[*].FEATURE ANN[*].FEATUREID ANN[*].IMPACT ANN[*].BIOTYPE ANN[*].HGVS_C ANN[*].HGVS_P LOF"
SNPSIFT_FILTER_SNPEFF="CHROM POS REF ALT ANN"
extract_annot "$SNPSIFT_FILTER_SNPEFF" "snpeff" "$RESULTS/vardict/snpeff" "$RESULTS/vardict/vardict_snpeff.tsv"
extract_annot "$SNPSIFT_FILTER_SNPEFF" "snpeff" "$RESULTS/lofreq/snpeff" "$RESULTS/lofreq/lofreq_snpeff.tsv"
extract_annot "$SNPSIFT_FILTER_SNPEFF" "snpeff" "$RESULTS/snver/snpeff" "$RESULTS/snver/snver_snpeff.tsv"
cat "$RESULTS/vardict/vardict_snpeff.tsv" "$RESULTS/lofreq/lofreq_snpeff.tsv" "$RESULTS/snver/snver_snpeff.tsv" > "$RESULTS/snpeff_all.tsv"
cat "$RESULTS/vardict/vardict_snpeff.tsv" "$RESULTS/lofreq/lofreq_snpeff.tsv" "$RESULTS/snver/snver_snpeff.tsv" | cut -f2- | sort | uniq > "$RESULTS/snpeff.tsv"
cat <(grep "^SAMPLE" "$RESULTS/snpeff_all.tsv" | head -1) <(grep -v "^SAMPLE" "$RESULTS/snpeff_all.tsv") | sponge "$RESULTS/snpeff_all.tsv"
cat <(grep "^CHROM" "$RESULTS/snpeff.tsv" | head -1) <(grep -v "^CHROM" "$RESULTS/snpeff.tsv") | sponge "$RESULTS/snpeff.tsv" 




# get Clinvar
SNPSIFT_FILTER_CLINVAR="CHROM POS REF ALT ID CLNSIG"
extract_annot "$SNPSIFT_FILTER_CLINVAR" "clinvar" "$RESULTS/vardict/clinvar" "$RESULTS/vardict/vardict_clinvar.tsv"
extract_annot "$SNPSIFT_FILTER_CLINVAR" "clinvar" "$RESULTS/lofreq/clinvar" "$RESULTS/lofreq/lofreq_clinvar.tsv"
extract_annot "$SNPSIFT_FILTER_CLINVAR" "clinvar" "$RESULTS/snver/clinvar" "$RESULTS/snver/snver_clinvar.tsv"
cat "$RESULTS/vardict/vardict_clinvar.tsv" "$RESULTS/lofreq/lofreq_clinvar.tsv" "$RESULTS/snver/snver_clinvar.tsv" | cut -f2- | sort | uniq > "$RESULTS/clinvar.tsv"
cat <(grep "^CHROM" "$RESULTS/clinvar.tsv") <(grep -v "^CHROM" "$RESULTS/clinvar.tsv") | sponge "$RESULTS/clinvar.tsv"


# get dbsnp
SNPSIFT_FILTER_DBSNP="CHROM POS REF ALT ID"
extract_annot "$SNPSIFT_FILTER_DBSNP" "dbsnp" "$RESULTS/vardict/dbsnp" "$RESULTS/vardict/vardict_dbsnp.tsv"
extract_annot "$SNPSIFT_FILTER_DBSNP" "dbsnp" "$RESULTS/lofreq/dbsnp" "$RESULTS/lofreq/lofreq_dbsnp.tsv"
extract_annot "$SNPSIFT_FILTER_DBSNP" "dbsnp" "$RESULTS/snver/dbsnp" "$RESULTS/snver/snver_dbsnp.tsv"
cat "$RESULTS/vardict/vardict_dbsnp.tsv" "$RESULTS/lofreq/lofreq_dbsnp.tsv" "$RESULTS/snver/snver_dbsnp.tsv" | cut -f2- | sort | uniq > "$RESULTS/dbsnp.tsv"
cat <(grep "^CHROM" "$RESULTS/dbsnp.tsv") <(grep -v "^CHROM" "$RESULTS/dbsnp.tsv") | sponge "$RESULTS/dbsnp.tsv"

# get cosmic
SNPSIFT_FILTER_COSMIC="CHROM POS REF ALT ID"
extract_annot "$SNPSIFT_FILTER_COSMIC" "cosmic" "$RESULTS/vardict/cosmic" "$RESULTS/vardict/vardict_cosmic.tsv"
extract_annot "$SNPSIFT_FILTER_COSMIC" "cosmic" "$RESULTS/lofreq/cosmic" "$RESULTS/lofreq/lofreq_cosmic.tsv"
extract_annot "$SNPSIFT_FILTER_COSMIC" "cosmic" "$RESULTS/snver/cosmic" "$RESULTS/snver/snver_cosmic.tsv"
cat "$RESULTS/vardict/vardict_cosmic.tsv" "$RESULTS/lofreq/lofreq_cosmic.tsv" "$RESULTS/snver/snver_cosmic.tsv" | cut -f2- | sort | uniq > "$RESULTS/cosmic.tsv"
cat <(grep "^CHROM" "$RESULTS/cosmic.tsv") <(grep -v "^CHROM" "$RESULTS/cosmic.tsv") | sponge "$RESULTS/cosmic.tsv"

echo
echo "Done"
}


main
