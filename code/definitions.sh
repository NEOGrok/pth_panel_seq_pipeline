# extra output/echo
DEBUG=false



function mk_dir () {
    DIR=$1
    if [ ! -d "$DIR" ]; then
    	echo "creating $DIR"
        mkdir -p "$DIR"
    fi
}


MAIN=".."
CODE=$MAIN/code
META=$MAIN/meta  # metadata, sample_spec probe and target regions, must be created manually
BATCHES=$MAIN/batches
BATCH=$BATCHES/$B
SAMPLE_INFO=$META/"sample_info.tsv"  # created manually
FQ=$MAIN/fq  	# create manually, contains the fq.gz files that the sample spec refers to

LOCK=$BATCH/lock
LOG=$BATCH/log
RESULTS=$BATCH/results
RESULTS_MAIN=$BATCHES/results
REFS=$MAIN/refs
TMPDIR=$MAIN/tmp/


TOP_FOLDERS=($LOCK $LOG $RESULTS)
RUN_FOLDERS=($LOCK $LOG)
TOOL_FOLDERS=(aln dup bqsr qc ism hsm)
RESULT_FOLDERS=(vardict lofreq snver) 
RESULT_SUBFOLDERS=(raw norm dbsnp cosmic clinvar snpeff vep)
RESULTS2=(pindel scanitd cnacs softclip variant_filt cnacs)



egrep "\b$B\b" $SAMPLE_INFO
if [ ! $? ]; then
echo "$B doesn't exist in the sample spec."
exit 10
else
echo "exists"
fi

#
# ############################# PROGRAM VARIABLE DEFINITIONS #############################
#

BWA=bwa
SAMTOOLS=samtools
SAMBAMBA=sambamba
PICARD=picard-2.6.0
GATK=gatk

SOFTCLIPSPIKES_R=$MAIN/code/softclip_spikes.R



SNPEFF_DIR=/home/francisc/bin/snpEff
SNPEFF=(java -Xmx6g -jar ${SNPEFF_DIR}/snpEff.jar GRCh38.86)

SNPSIFT=(java -jar ${SNPEFF_DIR}/SnpSift.jar)
ANNOT=($SNPSIFT annotate)
FILT=($SNPSIFT filter)
EXTR=($SNPSIFT extractFields)

SNPEFF_SCRIPTS="${SNPEFF_DIR}/scripts"

VEP_CACHE="/home/francisc/share/vep_cache"

#
# ############################# VARIABLE SETUP #############################
#
REF_GEN=/home/francisc/data/refs/hs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly.fa
BWA_INDEX=$REF_GEN

# Annotation DBs
DBSNP=/home/francisc/data/refs/hs/var/00-All.vcf.gz
COSMIC=/home/francisc/data/refs/hs/var/Cosmic_all.vcf.gz
CLINVAR=/home/francisc/data/refs/hs/var/clinvar.vcf.gz




# Probe and Target Intervals

# TBD select which panel version... or manually add them and expect a certain naming?
# little panel
#BAIT_INTERVALS_BED=$META/Focused_myeloid_panel-Probe_placement_file-TE-93310852_hg38_v2_190722165707.bed
#TARGET_INTERVALS_BED=$META/Focused_myeloid_panel-All_target_segments_covered_by_probes-TE-93310852_hg38_v2_190722165759.bed
#
## large panel v1
#BAIT_INTERVALS_BED=$META/ProbePlacement_Schmidt_Myeloid_TE-98545653_hg38.bed
#TARGET_INTERVALS_BED=$META/all_target_segments_covered_by_probes_Schmidt_Myeloid_TE-98545653_hg38.bed

# Large Panel Bait + Target Intervals v2
BAIT_INTERVALS_BED=$META/probes_combined_PTH_Comprehensive_Myeloid_Panel_v2_2_TE-98216079_hg38_with-NPM1-ins-probes.bed
TARGET_INTERVALS_BED=$META/all_target_segments_covered_by_probes_Schmidt_Myeloid_TE-98545653_hg38_190919225222_updated-to-v2.bed
BAIT_INTERVALS=${BAIT_INTERVALS_BED%.bed}.int_list
TARGET_INTERVALS=${TARGET_INTERVALS_BED%.bed}.int_list
REGIONS=$META/REGIONS_slop300.bed
REGIONS_CHR=$META/REGIONS_slop300_chr.bed


