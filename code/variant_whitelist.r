# TBD should I include ref alt?
variant_whitelist <- tribble(
  ~chr, ~start, ~ref, ~alt,
  "9", 5073770, "G", "T",   # JAK2  V617F 9 5073770 G T COSM12600
  "13", 28034079, "A", "<DUP>"   # PTH0002 FLT3 ITD
)
variant_whitelist %<>% mutate(variant = str_c(chr, start, ref, alt, sep = "_"))

