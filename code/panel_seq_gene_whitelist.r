variant_whitelist <- tribble(
  ~chr, ~start,
  9, 5073770,    # JAK2  V617F 9 5073770 G T COSM12600
  13, 28034079   # PTH0002 FLT3 ITD
)
