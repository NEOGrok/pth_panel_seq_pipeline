#!/usr/bin/env Rscript
#rm(list = ls())


# get options, using the spec as defined by the enclosed list.
# we read the options from the default: commandArgs(TRUE).
args = commandArgs(trailingOnly = TRUE)

P_MAIN <- args[1L]

message(   
  sprintf("arg1 = %s\n", args[1L])
)


message(Sys.time(), ": Starting variant filtration pipeline...")

#
# source files
#
P_CODE = file.path(P_MAIN, "code")
P_RESULTS = file.path(P_MAIN, "results")
P_META = file.path(P_MAIN, "meta")
P_OUT = file.path(P_RESULTS, "variant_filt")  # results/var_filt

print(P_CODE)

source(file.path(P_CODE, "panel_seq_libraries.r"))
source(file.path(P_CODE, "panel_seq_functions.r"))
source(file.path(P_CODE, "panel_seq_data.r"))
#source(file.path(P_CODE, "panel_seq_gene_whitelist.r"))
source(file.path(P_CODE, "maf_assignment.r"))
source(file.path(P_CODE, "variant_whitelist.r"))
source(file.path(P_CODE, "panel_seq_run.r"))
source(file.path(P_CODE, "panel_seq_write_data.r"))
source(file.path(P_CODE, "panel_seq_plots.r"))



message(Sys.time(), ": Filtering Done.")

# R_DAT <- file.path(P_RDAT, "loaded_filtered_variant_annotations.RData")
# if (file.exists(R_DAT)) {
#   cat("Loading ", R_DAT)
#   load(R_DAT)
# } else {
# }

