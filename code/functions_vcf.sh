#!/usr/bin/env bash

set -ueo pipefail




function run_vardict () {
    local in_bam="$1"
    local vardict_dir="$2"
    local sample="$3"

    local out_vcf="$vardict_dir/raw/${sample}.vcf"

    echo
    echo "## Vardict"
    echo "## $in_bam --> $out_vcf"

    if [ $DEBUG == "false" ]; then
    local JAVA_OPTS="-Xms2048m -Xmx12g"
    local MIN_AF=0.01
    local TUMOR_NAME=tumor

    echo
    echo "# Running vardict on $in_bam"

    if [ ! -f ${out_vcf}.gz ]; then
    echo "vardict"
    vardict-java \
    -G $REF_GEN \
    -N $sample \
    -f $MIN_AF \
    -b $in_bam \
    -c 1 -S 2 -E 3 -g 4 -th 2 \
    $REGIONS \
    | teststrandbias.R \
    | var2vcf_valid.pl -N $TUMOR_NAME -E -f $MIN_AF \
    | awk '{if ($1 ~ /^#/) print; else if ($4 != $5) print}' > $out_vcf
    fi


    normalize_n_annotate $out_vcf $vardict_dir $sample
    fi
}





function run_lofreq () {
    local in_bam="$1"
    local lofreq_dir="$2"
    local sample="$3"

    local out_vcf=$lofreq_dir/raw/${sample}.vcf
    local bam_realn=$TMPDIR/${sample}_realn.bam
    local bam_tmp=$TMPDIR/${sample}_tmp.bam
    local bam_iq=$TMPDIR/${sample}_iq.bam

    echo
    echo "## Lofreq"
    echo "## $in_bam --> $out_vcf"

    if [ $DEBUG == "false" ]; then
    if [ ! -f $bam_realn ]; then
        echo "realigning"
        lofreq viterbi \
        -f $REF_GEN \
        -o $bam_realn \
        $in_bam
    fi


    if [ ! -f $bam_iq ]; then
        echo "adding indel qualities"
        lofreq indelqual \
        --dindel \
        -f $REF_GEN \
        -o $bam_tmp \
        $bam_realn

        $SAMBAMBA sort -t 2 -o $bam_iq $bam_tmp  # sort and index
    fi


    
    if ! [ -f $out_vcf ] && ! [ -f ${out_vcf}.gz ]; then
        echo "calling variants"
        lofreq call-parallel \
        --verbose \
        --call-indels \
        --min-cov 10 \
        --no-default-filter \
        -f $REF_GEN \
        -l $REGIONS \
        --pp-threads 2 \
        -o $out_vcf \
        $bam_iq


        #echo "filtering"
#        lofreq filter \
#        --verbose \
#        -i $out_vcf \
#        -o ${out_vcf%.vcf}_filt.vcf
    fi

    normalize_n_annotate $out_vcf $lofreq_dir $sample
fi
}


function run_snver () {
    local in_bam="$1"
    local snver_dir="$2"
    local sample="$3"

    echo
    echo "## Snver"
    echo "## $in_bam"

    if [ $DEBUG == "false" ]; then
    if ! [ -f $snver_dir/raw/${sample}.filter.vcf.gz ] && ! [ -f $snver_dir/raw/${sample}.indel.filter.vcf.gz ]; then
        snver \
        -i $in_bam \
        -o $snver_dir/raw/$sample \
        -r $REF_GEN \
        -l $REGIONS \
        -b 0.01
    fi

    normalize_n_annotate $snver_dir/raw/${sample}.filter.vcf $snver_dir ${sample}
    normalize_n_annotate $snver_dir/raw/${sample}.indel.filter.vcf $snver_dir ${sample}.indel
    fi
}


function run_pindel () {
    local in_bam="$1"
    local pindel_dir="$2"
    local ism_out="$3"
    local sample="$4"
    local out_prefix=$pindel_dir/$sample
    echo "$ism_out"

    echo
    echo "## Pindel"
    echo "## $in_bam"

    if [ $DEBUG == "false" ]; then
    ins_size=$(grep -A2 "^## METRICS CLASS" $ism_out | sed -n '3p' | awk '{print $1}')
    echo "$ins_size"
    echo -en "$in_bam\t$ins_size\t$sample" > $TMPDIR/${sample}_config.txt
    #grep "$in_bam" bam_config.txt > tmp_config.txt

    pindel \
    -f $REF_GEN \
    -i $TMPDIR/${sample}_config.txt \
    -c ALL \
    -T 2 \
    -o $out_prefix
    

    for f in ${out_prefix}_*; do
    echo "pindel2vcf: $f"
    pindel2vcf -p $f -r $REF_GEN -R GRCh38 -d 01012019
    done
    
    fi
}



function run_scanitd () {
    local in_bam="$1"
    local scanitd_dir="$2"
    local sample="$3"

    local out_vcf=$scanitd_dir/${sample}.vcf

	ScanITD.py -i $in_bam -n 3 -r $REF_GEN -t $REGIONS -o $out_vcf
}




function soft_clip_spikes () {
	local in_bam="$1"
	local softclip_dir="$2"
	local sample="$3"

	local sample_chr_pos_cigar_seq=$softclip_dir/${sample}_spcs.tsv

	# extract pos cigar seq from BAM from the FLT3 region
	echo "Extracting FLT3 data from $sample"
	FLT3ITD="13:28033800-28034200"
	#samtools view $in_bam $FLT3ITD | cut -f3,4,6,10 | awk -v s=$sample -v OFS="\t" -v refg=$REF_GEN '{print s, $1, $2, $3, $4}' > $sample_chr_pos_cigar_seq
samtools view $in_bam $FLT3ITD | cut -f3,4,6,10 | awk -v s=$sample -v OFS="\t" '{print s, $1, $2, $3, $4}' | while read s r p c q; do echo -ne "$s\t$r\t$p\t"; samtools faidx $REF_GEN $r:$p-$p | tail -1 | tr -d "\n"; echo -e "\t.\t$c\t$q"; done > $sample_chr_pos_cigar_seq

	# call spike finding R script
	echo "Finding SoftClip Spikes for $sample"
	MIN_CIGAR_SPIKE=15
	Rscript ${SOFTCLIPSPIKES_R} $sample $softclip_dir $sample_chr_pos_cigar_seq $MIN_CIGAR_SPIKE $REF_GEN
}



function run_strelka2 {
  local in_bam="$1"
  local out_dir="$2"

  echo "Generating workflow"
  $STRELKA2_GERMLINE \
  --bam="$in_bam" \
 --referenceFasta="$REF_GEN" \
  --callMemMb=1024 \
  --disableSequenceErrorEstimation \
  --runDir="$out_dir" \
  --callRegions=$REGIONS


  echo "Running workflow"
  $out_dir/runWorkflow.py -m local -j 6 -g 8
}




function run_haplotypecaller () {
  local in_bam="$1"
  local out_vcf="$2"

	if [ ! -f $out_vcf ]; then
	gatk HaplotypeCaller \
	-I $in_bam \
	-R $REF_GEN \
	-O $out_vcf \
	-L $REGIONS
	fi
}


function normalize_n_annotate () {
    local in_vcf=$1
    local out_dir="$2"
    local sample="$3"

    echo "## Norm and Annot"
    echo "## $in_vcf"

    if [ ! -f ${in_vcf}.gz ]; then
        bgzip $in_vcf
    fi
    
    sorted_vcf=${in_vcf%.vcf*}_sort.vcf.gz
    zcat ${in_vcf}.gz | awk '$1 ~ /^#/ {print $0;next} {print $0 | "sort -k1,1V -k2,2n"}' | bgzip > $sorted_vcf

    if [ ! -f ${sorted_vcf}.tbi ]; then
        tabix -f $sorted_vcf
    fi

    norm_vcf=$out_dir/norm/${sample}_norm.vcf.gz
    if ! [ -f $norm_vcf ]; then
    echo "Normalizing $in_vcf"
    bcftools norm -m-any -f $REF_GEN -Oz $sorted_vcf > $norm_vcf && tabix $norm_vcf
    fi
    

    annotate_vcf "$norm_vcf" $out_dir $sample
}




function annotate_vcf () {
    local in_vcf=$1
    local out_dir=$2
    local sample="$3"

    echo $in_vcf
    echo $out_dir

    # DBSNP
    echo "annotating $in_vcf with DBSNP"
    dbsnp_vcf=$out_dir/dbsnp/${sample}_dbsnp.vcf.gz
    if ! [ -f $dbsnp_vcf ]; then
    bcftools annotate -c ID -a $DBSNP -R $REGIONS $in_vcf -Oz -o $dbsnp_vcf
    fi


    # COSMIC
    echo "annotating $in_vcf with COSMIC"
    cosmic_vcf=$out_dir/cosmic/${sample}_cosmic.vcf.gz
    if ! [ -f $cosmic_vcf ]; then
    bcftools annotate -c ID -a $COSMIC -R $REGIONS $in_vcf -Oz -o $cosmic_vcf
    fi

    # snpEff functional annotation
    echo "annotating $in_vcf with snpEff"
    snpeff_vcf=$out_dir/snpeff/${sample}_snpeff.vcf
    if ! [ -f ${snpeff_vcf}.gz ]; then
    snpEff GRCh38.86 $in_vcf > $snpeff_vcf
	gzip $snpeff_vcf
    fi

    # VEP annotation
    echo "VEP annotating $in_vcf"
    vep_vcf=$out_dir/vep/${sample}_vep.vcf.gz
    if [ ! -f $vep_vcf ]; then
    vep --refseq --symbol --terms SO --tsl \
	--hgvs --fasta $REF_GEN \
	--pick_allele_gene \
	--check_existing \
	--cache --dir_cache "$VEP_CACHE" \
	--distance 0 \
	--format vcf --vcf --compress_output gzip \
  -i "$in_vcf" -o "$vep_vcf"
    fi

	# clinvar: known clinical annotation
	echo "annotating $in_vcf with clinvar"
	clinvar_vcf=$out_dir/clinvar/${sample}_clinvar.vcf.gz
	if ! [ -f ${clinvar_vcf} ]; then
	bcftools annotate -a $CLINVAR -c ID,CLNSIG -R $REGIONS $in_vcf -Oz -o $clinvar_vcf
	fi
}


function call_var () {
IN_BAM=$1
OUT_VCF=$2
CALLER=$3

P_OUT=${OUT_VCF%/*}
#mk_dir $P_OUT


echo "$IN_BAM"

case $CALLER in
    "VARDICT" )
        run_vardict $IN_BAM $OUT_VCF ;;
    "LOFREQ" )
        run_lofreq $IN_BAM $OUT_VCF ;;
    "SNVER" )
        run_snver $IN_BAM $OUT_VCF ;;
    "PINDEL" )
        echo "pindel" ;;
    "HAPLOTYPECALLER" )
        run_haplotypecaller $IN_BAM $OUT_VCF ;;
    "FREEBAYES" )
        echo "freebayes" ;;
    "NORM_ANNOT" )
        normalize_n_annotate $IN_BAM ;;
    * )
        echo "WTF" ;;
esac
}



