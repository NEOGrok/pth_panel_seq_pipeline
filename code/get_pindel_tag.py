#!/usr/bin/env python
# gt2num.py

from sys import stdin, stdout, argv
import numpy as np




def main():

    toi = argv[1:]  # tags of interest


    while True:
        #print(".")
        line = stdin.readline().strip().split()
        #print("..")
        if not line:
            break

        #print(toi)
        #print(line)
        #print("...")
        print(line)
        
        tags = [x for x in line[5].split(";") if x.split('=')[0] in toi]
        tags = [x.split('=')[1] for x in tags]
        #print(tags)
        
        fields = line[0:5] # + [line[9]]  # read name list (1st element) + list with chr name (2nd element)
        ad = [float(x) for x in line[6].split(":")[1].split(",")]
        dp = ad[0] + ad[1]
        if dp == 0:
            vaf = 0
        else:
            vaf = round(ad[1] / dp, 3)
        vaf_info_str = [str(x) for x in [dp, ad[1], vaf]]
        #print(fields)
        #out_line = "\t".join(fields + tags)
        out_line = "\t".join(fields + vaf_info_str + tags)
        #out_line = "\t".join(tags.insert(0,fields))
        #out_line = "\t".join(map(lambda g: gts[g], line.split()))
        stdout.write("%s\n" % out_line)
        stdout.flush()


if __name__ == "__main__":
    main()

