

message(Sys.time(), ": Generating plots...")


#
# Stat Plots
#

# pdf_name <- file.path(P_PLOTS, "plots_variant_stats.pdf")
# pdf(pdf_name)
# 
# F_SAMPLE_INFO %>%
#   group_by(batch, type, panel) %>% tally %>%
#   ggplot(aes(x=batch, y=n, alpha = type, color = panel)) +
#   geom_col(position = position_dodge()) +
#   scale_y_continuous(breaks=1:11) +
#   theme_minimal()
# 
# dev.off()
# 
# 


{
pdf_name <- file.path(P_PLOTS, "various.pdf")
pdf(pdf_name)


#
# Insert Size Metrics
#


ISM <- read_tsv(file.path("/Users/zhc273/work/PTH_panel/stats/ism_hist.tsv"), col_names = c("sample", "insert_size", "number_reads"), col_types = "cnn")

plt <- ISM %>% separate(sample, c("sample", "panel"), sep = "-C") %>%
  mutate(panel = str_c("C", panel)) %>%
  #filter(panel == "CMP008") %>%   # comment out for all
  ggplot(aes(x = insert_size, y = number_reads, color = sample)) +
  facet_wrap(~panel) +
  theme_bw() +
  xlim(50,400)

print(plt + geom_line(show.legend = F, size = 0.5))
#girafe(ggobj = plt + geom_line_interactive(size = 0.5, aes(tooltip = sample, data_id = sample), show.legend = F))









#
# HS Metrics
#
# t_avg_cov <- read_tsv("/Users/zhc273/work/PTH_panel/target_avg_cov.tsv")
# t_avg_cov <- read_tsv("/Users/zhc273/work/PTH_panel/target_avg_cov.tsv", skip = 1, col_names = colnames(t_avg_cov), col_types = str_c(c("ciii", str_c(rep("d", 162))), collapse = ""))
# 
HS_METRICS <- read_tsv("/Users/zhc273/work/PTH_panel/stats/hs_metrics.tsv", col_names = c("sample", "metric", "value"), col_types = "ccn")


hsm_p <- HS_METRICS %>% filter(metric == "TOTAL_READS") %>%
  spread(key = "metric", value = "value") %>%
  mutate(sample = str_replace(sample, "_.*", "")) %>%
  inner_join(F_SAMPLE_INFO) %>%
  mutate(batch = ordered(batch),
         sample = fct_reorder(sample, batch, min)) %>%
  
  ggplot(aes(x = sample, y = TOTAL_READS, fill = batch)) +
  geom_col() +
  #geom_text(aes(label = sample), size = 2.3, color = "darkgrey") +
  #scale_color_manual(values=cmp_colors) +  #ylab("PCT_OFF_BAIT") +
  scale_y_continuous(labels = scales::comma) +
  #facet_wrap(~batch, scales = "free") +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  )
print(hsm_p)


hsm_p <- HS_METRICS %>% filter(metric == "MEAN_TARGET_COVERAGE") %>%
  spread(key = "metric", value = "value") %>%
  mutate(sample = str_replace(sample, "_.*", "")) %>%
  inner_join(F_SAMPLE_INFO) %>%
  
  ggplot(aes(x = sample, y = MEAN_TARGET_COVERAGE, color = panel, shape = batch)) +
  geom_point(size = 2.5) +
  #geom_text(aes(label = sample), size = 2.3, color = "darkgrey") +
  #scale_color_manual(values=cmp_colors) +  #ylab("PCT_OFF_BAIT") +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  )
print(hsm_p)


hsm_p <- HS_METRICS %>% filter(metric == "PCT_OFF_BAIT") %>%
  mutate(sample = str_replace(sample, "_sort.*", "")) %>%
  inner_join(F_SAMPLE_INFO %>% unique, by = "sample") %>%
  mutate(batch = ordered(batch), sample = fct_reorder(sample, batch, min)) %>%
  
  ggplot(aes(x = sample, y = as.numeric(value), fill = batch)) +
  geom_col() +
  scale_x_discrete(name = "Sample") +
  ylab("PCT_OFF_BAIT") +
  ylim(0,1) +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  ) +
  ggtitle(label = HS_METRICS$metric[1])
print(hsm_p)




#
# Variant Stats
#

var_p <- comb %>% #select(sample, variant, caller) %>%
  group_by(sample, caller) %>%
  mutate(n = n()) %>%
  select(sample, caller, n) %>%
  ungroup %>%
  unique %>%
  
  inner_join(F_SAMPLE_INFO %>% unique, by = "sample") %>%
  
  mutate(batch = ordered(batch),
         sample = fct_reorder(sample, batch, min)) %>%
  
  
  ggplot(aes(x = sample, y = n, fill = batch)) +
  geom_col() +
  facet_wrap(~caller) +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  ) 
print(var_p)





var_p <- comb_filt %>% #select(sample, variant, caller) %>%
  group_by(sample, caller) %>%
  mutate(n = n()) %>%
  select(sample, caller, n) %>%
  ungroup %>%
  unique %>%
  
  inner_join(F_SAMPLE_INFO %>% unique, by = "sample") %>%
  
  mutate(batch = ordered(batch),
         sample = fct_reorder(sample, batch, min)) %>%
  
  
  ggplot(aes(x = sample, y = n, fill = batch)) +
  geom_col() +
  facet_wrap(~caller) +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  ) 


print(var_p)



#
# Variants per gene
#
vpg_p <- comb_filt %>%
  select(variant, SYMBOL) %>% unique %>%
  group_by(SYMBOL) %>%
  mutate(n = n()) %>%
  ungroup %>%
  select(SYMBOL, n) %>% unique %>%
  arrange(desc(n)) %>%
  mutate(SYMBOL = fct_reorder(SYMBOL, n, min)) %>%
  mutate(id = 1:n(), b = as.integer(id %/% 100)) %>%
  
  filter(n > 10) %>%

  ggplot(aes(x = SYMBOL, y = n)) +
  geom_col() +
  facet_wrap(~b, scales = "free") +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  ) 
print(vpg_p)


vpg_p <- comb_filt %>%
  select(variant, SYMBOL) %>% unique %>%
  group_by(SYMBOL) %>%
  mutate(n = n()) %>%
  ungroup %>%
  select(SYMBOL, n) %>% unique %>%
  arrange(desc(n)) %>%
  mutate(SYMBOL = fct_reorder(SYMBOL, n, min)) %>%
  mutate(id = 1:n(), b = as.integer(id %/% 200)) %>%
  
  filter(n <= 10) %>%
  
  filter(str_detect(SYMBOL, "LOC.*", negate = TRUE)) %>%
  
  ggplot(aes(x = SYMBOL, y = n)) +
  geom_col() +
  facet_wrap(~b, 2, 2, scales = "free") +
  theme(
    axis.text.x = element_text(angle = 90)#, legend.position = "none"
  ) 
print(vpg_p)



dev.off()


}

#
# PLOT Horizon Ref Variants
#

cat("Horison Reference Variants Plot\n")
P_PLOTS <- file.path(P_RESULTS, "plots")
if (!dir.exists(P_PLOTS)) { dir.create(P_PLOTS, recursive = TRUE) }

comb %>% plot_reference(D_HORIZION_REF, file.path(P_PLOTS, "comb_FCF_ref"))
comb_filt %>% plot_reference(D_HORIZION_REF, file.path(P_PLOTS, "comb_FCF_ref_filt"))
#cons_FCF %>% plot_reference(D_HORIZION_REF, "cons_FCF_ref")

plot_stats(comb, comb_filt, file.path(P_PLOTS, "comb_DP_VAF.pdf"))




#
# per gene plots
#
CHRS <- c(seq(1,22),'X','Y')

cat("Interactive variant per gene plots...\n")
P_PLOT_PER_GENE <- file.path(P_PLOTS, "per_gene")
if (!dir.exists(P_PLOT_PER_GENE)) { dir.create(P_PLOT_PER_GENE, recursive = TRUE) }

for (CHROM in CHRS) { 
  GENES <- comb_filt %>% filter(chrom == CHROM) %>% select(SYMBOL) %>% unique %>% filter(SYMBOL != "" & !is.na(SYMBOL)) %>% unlist
  for (GENE in GENES) {
    print(GENE)
    
    pp <- girafe(ggobj = plot_variants_chr(comb_filt %>% filter(SYMBOL == GENE), D_HORIZION_REF, CHROM, D_GOI_TRANSCRIPT$SYMBOL, GENE))
    
    str( tryCatch.W.E(
      htmlwidgets::saveWidget(file = file.path(P_PLOT_PER_GENE,
                                               str_c("combined_filt", GENE, ".html", sep = "_")),
                              girafe(print(pp), width_svg = 15))
    ))
  }
}








#
# Per Chromoome plots
#
cat("Interactive variant per chromosome plots...\n")
P_PLOTS_INTERACTIVE <- file.path(P_PLOTS, "interactive")
if (!dir.exists(P_PLOTS)) { dir.create(P_PLOTS_INTERACTIVE, recursive = TRUE) }

plots <- list()
for (chrom in CHRS) {
  print(chrom)
  p_dot <- plot_variants_chr(comb_filt, D_HORIZION_REF, chrom, D_GOI_TRANSCRIPT$SYMBOL)
  plots[[chrom]] <- p_dot
  
  str( tryCatch.W.E(
    htmlwidgets::saveWidget(file = file.path(P_PLOTS_INTERACTIVE, str_c("combined_filt", chrom, ".html", sep = "_")), girafe(print(p_dot), width_svg = 15))
  ))
}

pdf_name <- file.path(P_PLOTS, "plot_combined_filt.pdf")
pdf(pdf_name)
for (pp in plots) {
  print(pp)
}
dev.off()


















#
# Gene list plots
#


if (FALSE) {
  GENE_LIST_GREEN <- c(
    "DNMT3A",
    "TET2",
    "ASXL1",
    "RUNX1",
    "SRSF2",
    "BCORL1",
    "CBL"
  )
  
  
  GENE_LIST_YELLOW <- c(
    "CUX1",
    "CEBPA",
    "SH2B3",
    "STAG2",
    "SETBP1",
    "GATA2",
    "GNAS",
    "MPL"
  )
  
  
  GENE_LIST_RED <- c(
    "KMT2D",
    "CREBBP",
    "ARID1A",
    "ARID1B",
    "KMT2A",
    "KMT2C",
    "NF1",
    #  "RP11-2C24.9,SRCAP",
    "EP300",
    "NCOR2",
    "HIPK2",
    "ZNF318",
    "SF1",
    
    "NOTCH1",
    "YLPM1",
    
    "SMG1",
    "CSF3R",
    
    "GFI1",
    "DDX54",
    "PRPF8",
    
    "SMC3",
    "JARID2",
    "ATRX"
    
  )
  
  
  GENE <- "KMT2D"
  
  P_PLOTS_GENES <- file.path(P_PLOTS, "genes")
  if (!dir.exists(P_PLOTS_GENES)) { dir.create(P_PLOTS_GENES, recursive = TRUE) }
  
  pdf_name <- file.path(P_PLOTS, "plot_bar_recurrence_filt_green.pdf")
  pdf(pdf_name)
  for (GENE in GENE_LIST_GREEN) {
    bar_recurrence_plot <- comb_filt %>% filter(SYMBOL == GENE) %>% select(variant, recurrence, IMPACT, HGVSc, ID_DBSNP, ID_CLINVAR, ID_COSMIC) %>% unique %>% #arrange(desc(recurrence))
      ggplot(aes(x = variant, y = recurrence)) +
      geom_col_interactive( aes(tooltip = str_c(IMPACT, HGVSc, ID_DBSNP, ID_COSMIC, ID_CLINVAR, sep = "\n"), data_id = variant ), fill = "green", alpha = 0.6) +
      theme(
        axis.text.x = element_text(angle = 90, vjust = 0.5, size = 4),
        plot.margin = unit(rep(0.5, 4), "cm")
      ) + ggtitle(label = GENE)
    
    str( tryCatch.W.E(
      htmlwidgets::saveWidget(file = file.path(P_PLOTS_GENES, str_c("gene_investig", GENE, ".html", sep = "_")), girafe(print(bar_recurrence_plot)))
    ))
  }
  dev.off()
  
  
  pdf_name <- file.path(P_PLOTS, "plot_bar_recurrence_filt_yellow.pdf")
  pdf(pdf_name)
  for (GENE in GENE_LIST_YELLOW) {
    bar_recurrence_plot <- comb_filt %>% filter(SYMBOL == GENE) %>% select(variant, recurrence, INTRON, EXON, IMPACT, HGVSc, ID_DBSNP, ID_CLINVAR, ID_COSMIC) %>% unique %>% #arrange(desc(recurrence))
      ggplot(aes(x = variant, y = recurrence)) +
      geom_col_interactive( aes(tooltip = str_c(IMPACT, HGVSc, str_c("I", INTRON, sep = ":"), str_c("E", EXON, sep = ":"), ID_DBSNP, ID_COSMIC, ID_CLINVAR, sep = "\n"), data_id = variant ), fill = "yellow", alpha = 0.6) +
      theme(
        axis.text.x = element_text(angle = 90, vjust = 0.5, size = 4),
        plot.margin = unit(rep(0.5, 4), "cm")
      ) + ggtitle(label = GENE)
    str( tryCatch.W.E(
      htmlwidgets::saveWidget(file = file.path(P_PLOTS_GENES, str_c("gene_investig", GENE, ".html", sep = "_")), girafe(print(bar_recurrence_plot)))
    ))}
  dev.off()
  
  
  pdf_name <- file.path(P_PLOTS, "plot_bar_recurrence_filt_red.pdf")
  pdf(pdf_name)
  for (GENE in GENE_LIST_RED) {
    bar_recurrence_plot <- comb_filt %>% filter(SYMBOL == GENE) %>% select(variant, recurrence, IMPACT, HGVSc, ID_DBSNP, ID_CLINVAR, ID_COSMIC) %>% unique %>% #arrange(desc(recurrence))
      ggplot(aes(x = variant, y = recurrence)) +
      geom_col_interactive( aes(tooltip = str_c(IMPACT, HGVSc, ID_DBSNP, ID_COSMIC, ID_CLINVAR, sep = "\n"), data_id = variant ), fill = "red", alpha = 0.6) +
      theme(
        axis.text.x = element_text(angle = 90, vjust = 0.5, size = 4),
        plot.margin = unit(rep(0.5, 4), "cm")
      ) + ggtitle(label = GENE)
    str( tryCatch.W.E(
      htmlwidgets::saveWidget(file = file.path(P_PLOTS_GENES, str_c("gene_investig", GENE, ".html", sep = "_")), girafe(print(bar_recurrence_plot)))
    ))  #print(bar_recurrence_plot)
  }
  dev.off()
  
}
