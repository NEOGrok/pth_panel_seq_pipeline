#!/usr/bin/env bash
# Auth: FRK
# Description: map, sort, index, mark duplicates
# Date: 10.11.2020

set -ue


function map_bwa () {
i="$1"
o="$2"
s="$3"

echo
echo "## Mapping and Sorting" | tee -a out.txt
echo "## $i --> $o" | tee -a out.txt


if [ $DEBUG == "false" ]; then
if [ ! -f $o ]; then
R1=${i%,*}
R2=${i#*,}

ID=${s}  #.$t  # .$l 
LB=${s}  #.$t
SM=${s}  #.$t
PL=Illumina
PU=HCJLWAFX2  #.${l}
RG_ID="@RG\tID:${ID}\tLB:${LB}\tSM:${SM}\tPL:${PL}\tPU:${PU}"

tmp_unsorted_bam="$TMPDIR/tmp_${s}.bam"
echo "aligning in map_bwa" >> $TMPDIR/out.txt
echo "$tmp_unsorted_bam" | tee -a $TMPDIR/out.txt


# allow multiple mappings
#${BWA} mem -t 20 -T 20 -M -B 40 -O 60 -E 10 -L 100 \
${BWA} mem -t 2 -T 0 -M \
-R "@RG\tID:${ID}\tLB:${LB}\tSM:${SM}\tPL:${PL}" \
$BWA_INDEX $R1 $R2 | ${SAMBAMBA} view -t 2 -h -S -f bam /dev/stdin > $tmp_unsorted_bam

# picard sorts differently than samtools sort
echo "Sort and index"  # | tee -a $LOG_FILE
${SAMBAMBA} sort --tmpdir=$TMPDIR -t 2 -p $tmp_unsorted_bam -o $o
#${PICARD} SortSam O=${ALN_DIR}/${f}_sort.bam I=${ALN_DIR}/${f}.bam SORT_ORDER=queryname
else
    echo "$DEBUG"
fi
fi
}





function mark_dup () {
local bam_in="$1"
local bam_out="$2"

echo
echo  "## Mark Duplicates"
echo  "## $bam_in"

if [ $DEBUG == "false" ]; then
if [ ! -f $bam_out ]; then
${PICARD} MarkDuplicates \
I=$bam_in \
O=$bam_out \
M=${bam_out%.bam}_dup_metrics.txt \
TAGGING_POLICY=All \
OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500
fi
fi
}



function get_ism () {
    local bam_in=$1
    local out_ism=$2

    echo
    echo "## Collecting ISM"
    echo "## $bam_in"

    if [ $DEBUG == "false" ]; then
    ${PICARD} CollectInsertSizeMetrics INPUT=$bam_in HISTOGRAM_FILE=${out_ism%.out}.hist OUTPUT=$out_ism
    fi
}


function collect_hs_metrics () {
local bam_in=$1
local out_hsm=$2

echo
echo "## Collecting HS-Metrics"
echo "## $bam_in"

if [ $DEBUG == "false" ]; then
${PICARD} CollectHsMetrics \
I=$bam_in \
O=$out_hsm \
R=$REF_GEN \
PER_TARGET_COVERAGE=${out_hsm%.txt}_PTC.txt \
BAIT_INTERVALS=$BAIT_INTERVALS \
TARGET_INTERVALS=$TARGET_INTERVALS || echo "Failed"
fi
}






function run_bqsr () {
    local bam_in=$1
    local bam_out=$2

    echo
    echo "## BQSR"
    echo "## $bam_in" #| tee -a $LOG_FILE

    if [ $DEBUG == "false" ]; then
    if [ ! -f $bam_out ]; then
    BR_out=${bam_out%.bam}_BR.table
    gatk BaseRecalibrator \
    -I $bam_in \
    -R $REF_GEN \
    --known-sites $DBSNP \
    -O $BR_out

    # default setting (-OBI true) is to create BAM index
    gatk ApplyBQSR \
    -R $REF_GEN \
    -I $bam_in \
    --bqsr-recal-file $BR_out \
    -O $bam_out
    fi
    fi
}



