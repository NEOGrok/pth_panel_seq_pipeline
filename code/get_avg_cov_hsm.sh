
function get_avg_cov () {
echo "Concatenating avg cov..."
HSM_DIR="$1"
echo "A"
echo "B"
f1=$(ls -A $HSM_DIR/*hsm_PTC.txt | head -1)  # get 1st file name
echo $f1
#f2=${f1%.txt}_PTC.txt
OUT_FILE="$2"
echo "C"
echo "$OUT_FILE"
echo "D"
#echo "$f2"

# get all avg cov into one file
cut -f1-4 $f1 > $OUT_FILE # print chr start end len

eval $(echo paste $OUT_FILE $(ls -A $HSM_DIR/*hsm_PTC.txt | while read o; do echo -n "<(sed -n '1!p' $o | cut -f7 | awk -v S=$(basename ${o%_hsm*}) 'BEGIN{print S}{print}') "; done)) | sponge "$OUT_FILE"
}


get_avg_cov $1 $2
