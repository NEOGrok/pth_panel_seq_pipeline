message("Writing filtered variants as MAF formatted per patient files")


#
# MAF conversion... based on current naming
#   1st column (sample) isn't part of it and is used as a per-sample splitting variable
#

P_OUT_PERSAMPLE_MAF <- file.path(P_OUT, "per_sample")
if (!dir.exists(P_OUT_PERSAMPLE_MAF)) {
  dir.create(P_OUT_PERSAMPLE_MAF, recursive = TRUE)
}
comb_filt %>% out_2_MAF %>%
  group_by(sample) %>%
  group_split %>% setNames(comb_filt$sample %>% unique) %>%
  walk2(names(.), function(l,n) write_tsv(l %>% select(-sample), file.path(P_OUT_PERSAMPLE_MAF, str_c(n, "_filt.maf"))))






#
# variant recurrence
#
P_OUT_VAR_RECURRENCE <- file.path(P_OUT, "variant_recurrence.tsv")
comb_filt %>%
  select(variant, recurrence, SYMBOL, IMPACT, ID_DBSNP, ID_COSMIC, ID_CLINVAR, clinvar) %>% unique %>%
  #filter(recurrence > 10) %>%
  arrange(desc(recurrence)) %>%
  write_tsv(P_OUT_VAR_RECURRENCE)
  


