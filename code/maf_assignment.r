# We want the end result to be written in MAF format.
# In the script I use shorter column names, which must at the end be mapped to the corresponding MAF column name and position.

out_2_MAF <- function (dat) {
  # Description: create MAF format output from input data
  # Parameters: dat = variant filtered data from the pipeline with all the neccessary fields
  #   (global) MAF_format: tibble with MAF format column names and description. Defined in "panel_seq_data.r" 

out_MAF <- tibble(
  sample = dat$sample
)

if (dim(MAF_format)[1] != 126) {
  stop("MAF format is expected to be 126 dimensional.")
}

out_MAF %<>% add_column(!!(MAF_format$name[1]) := dat$SYMBOL)
out_MAF %<>% add_column(!!(MAF_format$name[2]) := as.integer(dat$Gene))
out_MAF %<>% add_column(!!(MAF_format$name[3]) := ".")
out_MAF %<>% add_column(!!(MAF_format$name[4]) := "GRCh38")
out_MAF %<>% add_column(!!(MAF_format$name[5]) := dat$chrom)  # Chromosome
out_MAF %<>% add_column(!!(MAF_format$name[6]) := dat$start)  # Start_Position
out_MAF %<>% add_column(!!(MAF_format$name[7]) := ".")  # End_Position 0: TBD TBD needs to be fixed
out_MAF %<>% add_column(!!(MAF_format$name[8]) := dat$STRAND) # Strand
out_MAF %<>% add_column(!!(MAF_format$name[9]) := ".")   # Variant_Classification
out_MAF %<>% add_column(!!(MAF_format$name[10]) := ".")  # Variant_Type
out_MAF %<>% add_column(!!(MAF_format$name[11]) := dat$ref)  # Reference_Allele
out_MAF %<>% add_column(!!(MAF_format$name[12]) := dat$alt)  # Tumor_Seq_Allele1    TBD: Is this what it's supposed to be?
out_MAF %<>% add_column(!!(MAF_format$name[13]) := ".")  # Tumor_Seq_Allele2        TBD:
out_MAF %<>% add_column(!!(MAF_format$name[14]) := dat$ID_DBSNP)  # dbSNP_RS
out_MAF %<>% add_column(!!(MAF_format$name[15]) := '.')	# dbSNP_Val_Status
out_MAF %<>% add_column(!!(MAF_format$name[16]) := '.')	# Tumor_Sample_Barcode
out_MAF %<>% add_column(!!(MAF_format$name[17]) := '.')	# Matched_Norm_Sample_Barcode
out_MAF %<>% add_column(!!(MAF_format$name[18]) := '.')	# Match_Norm_Seq_Allele1
out_MAF %<>% add_column(!!(MAF_format$name[19]) := '.')	# Match_Norm_Seq_Allele2
out_MAF %<>% add_column(!!(MAF_format$name[20]) := '.')	# Tumor_Validation_Allele1
out_MAF %<>% add_column(!!(MAF_format$name[21]) := '.')	# Tumor_Validation_Allele2
out_MAF %<>% add_column(!!(MAF_format$name[22]) := '.')	# Match_Norm_Validation_Allele1
out_MAF %<>% add_column(!!(MAF_format$name[23]) := '.')	# Match_Norm_Validation_Allele2
out_MAF %<>% add_column(!!(MAF_format$name[24]) := '.')	# Verification_Status
out_MAF %<>% add_column(!!(MAF_format$name[25]) := '.')	# Validation_Status
out_MAF %<>% add_column(!!(MAF_format$name[26]) := '.')	# Mutation_Status
out_MAF %<>% add_column(!!(MAF_format$name[27]) := '.')	# Sequencing_Phase
out_MAF %<>% add_column(!!(MAF_format$name[28]) := '.')	# Sequence_Source
out_MAF %<>% add_column(!!(MAF_format$name[29]) := '.')	# Validation_Method
out_MAF %<>% add_column(!!(MAF_format$name[30]) := '.')	# Score
out_MAF %<>% add_column(!!(MAF_format$name[31]) := '.')	# BAM_File
out_MAF %<>% add_column(!!(MAF_format$name[32]) := '.')	# Sequencer
out_MAF %<>% add_column(!!(MAF_format$name[33]) := '.')	# Tumor_Sample_UUID
out_MAF %<>% add_column(!!(MAF_format$name[34]) := '.')	# Matched_Norm_Sample_UUID
out_MAF %<>% add_column(!!(MAF_format$name[35]) := dat$HGVSc)	# HGVSc
out_MAF %<>% add_column(!!(MAF_format$name[36]) := dat$HGVSp)	# HGVSp
out_MAF %<>% add_column(!!(MAF_format$name[37]) := '.')	# HGVSp_Short
out_MAF %<>% add_column(!!(MAF_format$name[38]) := '.')	# Transcript_ID  (Ensembl)
out_MAF %<>% add_column(!!(MAF_format$name[39]) := dat$EXON)	# Exon_Number
out_MAF %<>% add_column(!!(MAF_format$name[40]) := dat$DP)	# t_depth
out_MAF %<>% add_column(!!(MAF_format$name[41]) := '.')	# t_ref_count
out_MAF %<>% add_column(!!(MAF_format$name[42]) := dat$AD)	# t_alt_count
out_MAF %<>% add_column(!!(MAF_format$name[43]) := '.')	# n_depth
out_MAF %<>% add_column(!!(MAF_format$name[44]) := '.')	# n_ref_count
out_MAF %<>% add_column(!!(MAF_format$name[45]) := '.')	# n_alt_count
out_MAF %<>% add_column(!!(MAF_format$name[46]) := '.')	# all_effects
out_MAF %<>% add_column(!!(MAF_format$name[47]) := '.')	# Allele
out_MAF %<>% add_column(!!(MAF_format$name[48]) := '.')	# Gene
out_MAF %<>% add_column(!!(MAF_format$name[49]) := '.')	# Feature
out_MAF %<>% add_column(!!(MAF_format$name[50]) := '.')	# Feature_type
out_MAF %<>% add_column(!!(MAF_format$name[51]) := '.')	# One_Consequence
out_MAF %<>% add_column(!!(MAF_format$name[52]) := '.')	# Consequence
out_MAF %<>% add_column(!!(MAF_format$name[53]) := dat$cDNA_position)	# cDNA_position
out_MAF %<>% add_column(!!(MAF_format$name[54]) := dat$CDS_position)	# CDS_position
out_MAF %<>% add_column(!!(MAF_format$name[55]) := dat$Protein_position)	# Protein_position
out_MAF %<>% add_column(!!(MAF_format$name[56]) := dat$Amino_acids)	# Amino_acids
out_MAF %<>% add_column(!!(MAF_format$name[57]) := dat$Codons)	# Codons
out_MAF %<>% add_column(!!(MAF_format$name[58]) := '.')	# Existing_variation
out_MAF %<>% add_column(!!(MAF_format$name[59]) := '.')	# ALLELE_NUM
out_MAF %<>% add_column(!!(MAF_format$name[60]) := '.')	# DISTANCE
out_MAF %<>% add_column(!!(MAF_format$name[61]) := '.')	# TRANSCRIPT_STRAND
out_MAF %<>% add_column(!!(MAF_format$name[62]) := '.')	# SYMBOL
out_MAF %<>% add_column(!!(MAF_format$name[63]) := '.')	# SYMBOL_SOURCE
out_MAF %<>% add_column(!!(MAF_format$name[64]) := '.')	# HGNC_ID
out_MAF %<>% add_column(!!(MAF_format$name[65]) := dat$BIOTYPE)	# BIOTYPE
out_MAF %<>% add_column(!!(MAF_format$name[66]) := '.')	# CANONICAL
out_MAF %<>% add_column(!!(MAF_format$name[67]) := '.')	# CCDS
out_MAF %<>% add_column(!!(MAF_format$name[68]) := '.')	# ENSP
out_MAF %<>% add_column(!!(MAF_format$name[69]) := '.')	# SWISSPROT
out_MAF %<>% add_column(!!(MAF_format$name[70]) := '.')	# TREMBL
out_MAF %<>% add_column(!!(MAF_format$name[71]) := '.')	# UNIPARC
out_MAF %<>% add_column(!!(MAF_format$name[72]) := dat$Feature)	# RefSeq
out_MAF %<>% add_column(!!(MAF_format$name[73]) := '.')	# SIFT
out_MAF %<>% add_column(!!(MAF_format$name[74]) := '.')	# PolyPhen
out_MAF %<>% add_column(!!(MAF_format$name[75]) := '.')	# EXON
out_MAF %<>% add_column(!!(MAF_format$name[76]) := dat$INTRON)	# INTRON
out_MAF %<>% add_column(!!(MAF_format$name[77]) := '.')	# DOMAINS
out_MAF %<>% add_column(!!(MAF_format$name[78]) := '.')	# GMAF
out_MAF %<>% add_column(!!(MAF_format$name[79]) := '.')	# AFR_MAF
out_MAF %<>% add_column(!!(MAF_format$name[80]) := '.')	# AMR_MAF
out_MAF %<>% add_column(!!(MAF_format$name[81]) := '.')	# ASN_MAF
out_MAF %<>% add_column(!!(MAF_format$name[82]) := '.')	# EAS_MAF
out_MAF %<>% add_column(!!(MAF_format$name[83]) := '.')	# EUR_MAF
out_MAF %<>% add_column(!!(MAF_format$name[84]) := '.')	# SAS_MAF
out_MAF %<>% add_column(!!(MAF_format$name[85]) := '.')	# AA_MAF
out_MAF %<>% add_column(!!(MAF_format$name[86]) := '.')	# EA_MAF
out_MAF %<>% add_column(!!(MAF_format$name[87]) := dat$clinvar)	# CLIN_SIG
out_MAF %<>% add_column(!!(MAF_format$name[88]) := '.')	# SOMATIC
out_MAF %<>% add_column(!!(MAF_format$name[89]) := '.')	# PUBMED
out_MAF %<>% add_column(!!(MAF_format$name[90]) := '.')	# MOTIF_NAME
out_MAF %<>% add_column(!!(MAF_format$name[91]) := '.')	# MOTIF_POS
out_MAF %<>% add_column(!!(MAF_format$name[92]) := '.')	# HIGH_INF_POS
out_MAF %<>% add_column(!!(MAF_format$name[93]) := '.')	# MOTIF_SCORE_CHANGE
out_MAF %<>% add_column(!!(MAF_format$name[94]) := dat$IMPACT)	# IMPACT
out_MAF %<>% add_column(!!(MAF_format$name[95]) := '.')	# PICK
out_MAF %<>% add_column(!!(MAF_format$name[96]) := '.')	# VARIANT_CLASS
out_MAF %<>% add_column(!!(MAF_format$name[97]) := '.')	# TSL
out_MAF %<>% add_column(!!(MAF_format$name[98]) := '.')	# HGVS_OFFSET
out_MAF %<>% add_column(!!(MAF_format$name[99]) := '.')	# PHENO
out_MAF %<>% add_column(!!(MAF_format$name[100]) := '.')	# MINIMISED
out_MAF %<>% add_column(!!(MAF_format$name[101]) := '.')	# ExAC_AF
out_MAF %<>% add_column(!!(MAF_format$name[102]) := '.')	# ExAC_AF_Adj
out_MAF %<>% add_column(!!(MAF_format$name[103]) := '.')	# ExAC_AF_AFR
out_MAF %<>% add_column(!!(MAF_format$name[104]) := '.')	# ExAC_AF_AMR
out_MAF %<>% add_column(!!(MAF_format$name[105]) := '.')	# ExAC_AF_EAS
out_MAF %<>% add_column(!!(MAF_format$name[106]) := '.')	# ExAC_AF_FIN
out_MAF %<>% add_column(!!(MAF_format$name[107]) := '.')	# ExAC_AF_NFE
out_MAF %<>% add_column(!!(MAF_format$name[108]) := '.')	# ExAC_AF_OTH
out_MAF %<>% add_column(!!(MAF_format$name[109]) := '.')	# ExAC_AF_SAS
out_MAF %<>% add_column(!!(MAF_format$name[110]) := '.')	# GENE_PHENO
out_MAF %<>% add_column(!!(MAF_format$name[111]) := '.')	# FILTER
out_MAF %<>% add_column(!!(MAF_format$name[112]) := '.')	# CONTEXT
out_MAF %<>% add_column(!!(MAF_format$name[113]) := '.')	# src_vcf_id
out_MAF %<>% add_column(!!(MAF_format$name[114]) := '.')	# tumor_bam_uuid
out_MAF %<>% add_column(!!(MAF_format$name[115]) := '.')	# normal_bam_uuid
out_MAF %<>% add_column(!!(MAF_format$name[116]) := '.')	# case_id
out_MAF %<>% add_column(!!(MAF_format$name[117]) := '.')	# GDC_FILTER
out_MAF %<>% add_column(!!(MAF_format$name[118]) := dat$ID_COSMIC)	# COSMIC
out_MAF %<>% add_column(!!(MAF_format$name[119]) := '.')	# MC3_Overlap
out_MAF %<>% add_column(!!(MAF_format$name[120]) := '.')	# GDC_Validation_Status
out_MAF %<>% add_column(!!(MAF_format$name[121]) := '.')	# GDC_Valid_Somatic
out_MAF %<>% add_column(!!(MAF_format$name[122]) := '.')	# vcf_region
out_MAF %<>% add_column(!!(MAF_format$name[123]) := '.')	# vcf_info
out_MAF %<>% add_column(!!(MAF_format$name[124]) := '.')	# vcf_format
out_MAF %<>% add_column(!!(MAF_format$name[125]) := '.')	# vcf_tumor_gt
out_MAF %<>% add_column(!!(MAF_format$name[126]) := '.')	# vcf_normal_gt

out_MAF %<>% add_column(variant_len = dat$variant_len) # extra column with variant length, e.g. FLT3 ITD length
out_MAF %<>% add_column(caller = dat$caller)	# vcf_normal_gt
#out_MAF %<>% select(-vid)

return(out_MAF)

}
