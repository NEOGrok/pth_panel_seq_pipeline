#!/usr/bin/env bash
# Auth: FRK
# Description: ...
# Date: 11.12.2020

set -ue



#
# ############################# COMMAND LINE ARGUMENTS #############################
#
# pipeline.sh -d /path/to/main_dir -n 10 (jobs)

while getopts hb:n: opt; do
    case $opt in
    h)
        echo "Usage: $0 -d /path/to/main_dir -n n_jobs"
        exit
        ;;
    b)
        B=$OPTARG
        echo "B = $B"
        ;;
    n)
		N_JOBS=$OPTARG
		echo "THREADS = $N_JOBS"
		;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        ;;
    esac
done


export $B

#
# ############################# DIRECTORY STRUCTURE SETUP #############################
#

. definitions.sh

echo
echo
echo "## $(date "+%H:%M:%S:") Creating folder structure..."
echo

mk_dir $TMPDIR
echo "$TMPDIR"
echo "$MAIN"

for T in ${TOP_FOLDERS[@]}; do
	echo "$T"
	mk_dir $T
done

for R in ${RUN_FOLDERS[@]}; do
	for S in ${TOOL_FOLDERS[@]}; do
		echo "$R/$S"
		mk_dir $R/$S
	done
done

for R in ${RESULT_FOLDERS[@]}; do
	echo "$R"
	mk_dir $RESULTS/$R
done

for R in ${RESULT_FOLDERS[@]}; do
	for S in ${RESULT_SUBFOLDERS[@]}; do
		echo "$RESULTS/$R/$S"
		mk_dir $RESULTS/$R/$S
	done
done


for R in ${RESULTS2[@]}; do
	echo "$R"
	mk_dir $RESULTS/$R
done






echo
echo
echo "## $(date "+%H:%M:%S:") Creating int_list files from target and probe files..."
echo
# Create .int_list from
${PICARD} BedToIntervalList \
I=$TARGET_INTERVALS_BED \
O=$TARGET_INTERVALS \
SD=${REF_GEN%.fa}.dict

${PICARD} BedToIntervalList \
I=$BAIT_INTERVALS_BED \
$RESULTSO=$BAIT_INTERVALS \
SD=${REF_GEN%.fa}.dict


# regions used in variant calling
echo
echo "## $(date "+%H:%M:%S:") Creating +-300bp regions file for variant calling..."
cat $BAIT_INTERVALS_BED $TARGET_INTERVALS_BED | sort -k1,1V -k2,2n | cut -f1-3 | bedtools merge -i - | bedtools slop -i - -b 300 -g ${REF_GEN%.fa}.dict > $REGIONS
$REGIONS_CHR=$(sed -E 's/^(.*)/chr\1/' $REGIONS)


#
# Create sample spec from FASTQ folder
echo
echo "## $(date "+%H:%M:%S:") Creating sample spec..."
#for f in $(ls -A $FQ/*.fq.gz | xargs -I{} -n1 basename {} | sed -n 's/_R..fq.gz//p' | sort | uniq); do echo -e "$MAIN\t$f"; done > $SAMPLE_SPEC


#
# ############################# MAIN LOOP #############################
#
exit 0

echo
echo
echo "## $(date "+%H:%M:%S:") Pre-processing"
#parallel --dry-run -a $SAMPLE_SPEC -j $N_JOBS --results $LOG "bash jobscript.sh {1} {2}"

parallel -u --verbose -C"\t" -a $SAMPLE_INFO -j $N_JOBS --results $LOG "bash jobscript.sh {1}"
echo
echo
echo "## $(date "+%H:%M:%S:") Middle-processing"
# Extract FLT3 ITD data, calls, and annotations
bash $CODE/04_process_variants.sh

# Get avg cov from HS-metrics files
echo
echo "- Extracting average coverage"
bash ./get_avg_cov_hsm.sh $LOCK/hsm $RESULTS/target_avg_cov.tsv # run on all samples at once... TBD

# Extract variant and annotation data for post-processing


echo
echo
echo "- Concatenating Batch Results"
# for each batch result
cat $BATCHES/B*/$RESULTS/calls.tsv > $RESULTS_MAIN/calls.tsv
cat $BATCHES/B*/$RESULTS/vep.tsv > $RESULTS_MAIN/vep.tsv
cat $BATCHES/B*/$RESULTS/snpeff.tsv > $RESULTS_MAIN/snpeff.tsv
cat $BATCHES/B*/$RESULTS/clinvar.tsv > $RESULTS_MAIN/clinvar.tsv
cat $BATCHES/B*/$RESULTS/dbsnp.tsv > $RESULTS_MAIN/dbsnp.tsv
cat $BATCHES/B*/$RESULTS/cosmic.tsv> $RESULTS_MAIN/cosmic.tsv


echo
echo
echo "## $(date "+%H:%M:%S:") Post-processing"
# given the output directory, this script reads in the necessary data,
# filters the variants, and generates a MAF formatted output per patient (patient report) + some plots
$CODE/variant_filtration.R "$MAIN"




echo
echo
echo "## $(date "+%H:%M:%S:") CNACS CNV caller"
bash $CODE/toil_cnacs.sh -d $RESULTS/bqsr -s $SAMPLE_INFO -o $RESULTS/cnacs -r $REGIONS_CHR -n 2
#bash $CODE/cnacs.sh $RESULTS/cnacs $SAMPLE_SPEC

