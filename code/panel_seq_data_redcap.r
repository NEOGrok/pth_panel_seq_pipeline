
#
# Load and arrange RedCap data
#



if (FALSE) { # LOAD
  # Load and prepare RedCap variants:
  #    since the info in the DB doesn't follow a strict structure it needs parsing and manual adjustment
  

  D_REDCAP <- read_tsv(file.path(P_DAT, "PTHDCCCInclusions-MutationsFromClinicW_DATA_2020-11-06_1330.csv"))
  D_REDCAP %>% dim
  D_REDCAP %>% glimpse
  
  # has max 4 non-empty gene groups so far...
  D_REDCAP <- bind_rows(
    D_REDCAP %>% dplyr::select(1,5:7) %>% rename_at(vars(starts_with("var_")), function(n) str_remove(n, "var_._")),
    D_REDCAP %>% dplyr::select(1,8:10) %>% rename_at(vars(starts_with("var_")), function(n) str_remove(n, "var_._")),
    D_REDCAP %>% dplyr::select(1,11:13) %>% rename_at(vars(starts_with("var_")), function(n) str_remove(n, "var_._")),
    D_REDCAP %>% dplyr::select(1,14:16) %>% rename_at(vars(starts_with("var_")), function(n) str_remove(n, "var_._"))
  ) %>% drop_na
  
  # get %VAFs out from the notes field for inspection
  D_REDCAP %>% dplyr::filter(str_count(notes, "%") > 1) %>%
    mutate(pcts = map_chr(notes, ~ str_c(str_extract_all(., "\\d*\\.?\\d+%")[[1]], collapse = ";"))) %>% View
  
  # Write to file and edit *manually*
  D_REDCAP %>% arrange(patient) %>% write_tsv(file.path(P_DAT, "RedCap_raw.tsv"))  # write and edit
}



D_REDCAP <- read_tsv(file.path(P_DAT, "RedCap_edited.tsv"))  # read edited version prepared above




# Edit patient name: PTHXXXX
D_REDCAP <- D_REDCAP %>%
  mutate(patient = str_c("PTH", map_chr(record_id, ~ str_c(rep("0", 4 - str_length(.)), collapse = "") ), record_id)) %>%
  
  mutate(transcr.ver = map_chr(notes, ~ str_extract(., "NM_\\d*\\.?\\d*")[[1]]),
         HGVSc = map_chr(notes, ~ str_c(unique(str_remove(str_extract_all(., "c\\.[^; ]+")[[1]], ";")), collapse = ",")),
         HGVSp = map_chr(notes, ~ str_c(str_remove(str_extract_all(., "p\\.\\S+")[[1]], ";"), collapse = ","))) %>%
  mutate(HGSVp = str_remove_all(HGVSp, "\\(|\\)")) %>%
  
  dplyr::select(patient, gene, transcr.ver, HGVSc, HGVSp, vaf) %>%
  
  # copy transcript name to entry where it's missin (NA)
  group_by(gene) %>%
  mutate(transcr.ver = ifelse(is.na(transcr.ver),
                              str_c(str_replace_na(unique(transcr.ver), replacement = ""), collapse = ""),
                              transcr.ver)) %>%
  ungroup %>%
  mutate(transcr.base = str_remove(transcr.ver, "\\.\\d")) %>% # add transcript name w.o. version
  
  mutate(vaf = as.double(vaf)/100) %>%
  
  arrange(transcr.ver) %>%
  mutate(VEP_in = str_c(transcr.ver, HGVSc, sep = ":"),
         VEP_in.base = gsub("(NM_\\d*)\\.\\d(:.*)", "\\1\\2", VEP_in)) %>%
  unique




# Get VEP annotated RedCap transcripts to get the HG38 locations of the NM_...c.hgvs SNPs:
D_REDCAP %>% filter(gene == "TET2") %>% select(VEP_in.base) %>% unique %>% View
D_REDCAP_VEP_patients %>% filter(gene == "TET2") %>% select(chrom, pos) %>% unique %>% View

D_REDCAP_VEP_in <- D_REDCAP %>%
  select(VEP_in) %>%
  unique %>%
  mutate(VEP_in.base = gsub("(NM_\\d*)\\.\\d(:.*)", "\\1\\2", VEP_in),
         feature_ver = replace_na(as.integer(gsub("NM_\\d*\\.(\\d):.*", "\\1", VEP_in)), 0)) %>%
  group_by(VEP_in.base) %>%
  mutate(n = length(VEP_in)) %>%
  arrange(desc(n)) %>%
  filter(feature_ver == max(feature_ver))

# after calling:
#  vep --refseq --symbol --terms SO --tsl \
# --hgvs --fasta $REFGENE \
# --pick_allele_gene \
# --check_existing \
# --cache --dir_cache "$VEP_CACHE" \
# --distance 0 \
# --format vcf --vcf --compress_output gzip \
# -i "$IN" -o "$OUT"

# D_REDCAP_VEP_out <- read_tsv(file.path(P_DAT, "redcap.outvep"), comment = "##") %>%
#   mutate(VEP_in = `#Uploaded_variation`, .keep = "unused") %>%
#   relocate(VEP_in, 1) %>%
#   separate(VEP_in, c("transcr.ver", "c.hgvs"), ":", remove = FALSE) %>%
#   dplyr::filter(str_detect(Feature, "^NM_")) %>%  # ignore XM_ ...
#   mutate(transcr.base = str_remove(transcr.ver, "\\.\\d"),
#          feature.base = str_remove(Feature, "\\.\\d")) %>%
#   dplyr::select(VEP_in, transcr.ver, transcr.base, Feature, feature.base, c.hgvs, Existing_variation, Location, Allele, Extra) %>%
#   #mutate(Extra = str_split(Extra, ";"), extra_n = map_int(Extra, function(x) length(x))) %>%
#   mutate(SYMBOL = gsub(".*SYMBOL=([^;]*).*", "\\1", Extra),
#          HGVSc = gsub(".*HGVSc=([^;]*).*", "\\1", Extra),
#          STRAND = gsub(".*STRAND=([^;]*).*", "\\1", Extra),
#          IMPACT = gsub(".*IMPACT=([^;]*).*", "\\1", Extra))
# 
# D_REDCAP_VEP_in %>% full_join(D_REDCAP_VEP_out, by = "VEP_in") %>% View
# 
# 


VCF_INFO_CSQ_COLS <- read_tsv(file.path(P_DAT, "redcap_vcf.outvep"), n_max = 3, col_names = F) %>%
  dplyr::slice(3) %>% unlist %>%
  str_remove_all(".*Format: |[^A-Z]*$") %>% str_split("\\|") %>% .[[1]]

D_REDCAP_VEP_out_VCF <- read_tsv(file.path(P_DAT, "redcap_vcf.outvep"), comment = "##") %>%
  rename(`#CHROM` = "CHROM", ID = "VEP_in") %>%
  mutate(INFO = str_remove(INFO, "^CSQ=")) %>%
  mutate(INFO = str_split(INFO, ",")) %>%
  unnest(INFO) %>%
  dplyr::filter(str_detect(INFO, "NM_")) %>%  # ignore XM_ ...
  separate(INFO, INFO_CSQ_COLS, "\\|") %>%
  separate(VEP_in, c("transcr.ver", "c.hgvs"), ":", remove = FALSE) %>%
  mutate(transcr.base = str_remove(transcr.ver, "\\.\\d"),
         feature.base = str_remove(Feature, "\\.\\d"),
         HGVSc.base = gsub("(NM_\\d*)\\.\\d(:.*)", "\\1\\2", HGVSc),
         VEP_in.base = gsub("(NM_\\d*)\\.\\d(:.*)", "\\1\\2", VEP_in),
         #Existing_variation = str_split(Existing_variation, "&"),
         ID_DBSNP = Existing_variation %>% map_chr(~str_extract_all(., "rs\\d*") %>% unlist %>% str_c(collapse = ",")),
         ID_COSMIC = Existing_variation %>% map_chr(~str_extract_all(., "COSV\\d*") %>% unlist %>% str_c(collapse = ","))) %>%
  filter(transcr.base == feature.base) %>%
  select(-c.hgvs) %>%
  
  filter(!str_detect(VEP_in, "NM_003016.4:c.419G>A"))  # wrong and corrected

str_extract_all( str_c(c("rs345", "COSV344", "rs987"), collapse = "&"), "rs\\d*") %>% unlist
str_match_all(c("rs345", "COSV344", "rs987"), "rs\\d*") %>% unlist
str_match_all(c("COSV343", "COSV344"), "rs\\d*") %>% unlist

D_REDCAP_VEP_in %>% full_join(D_REDCAP_VEP_out_VCF, by = "VEP_in") %>% View # %>% select(VEP_in) %>% unique %>% anti_join(D_REDCAP_VEP_in)
VEP_not_found <- D_REDCAP_VEP_in %>% anti_join(D_REDCAP_VEP_out_VCF, by = "VEP_in") %>% .$VEP_in %>% str_remove("NM_.*:")
D_REDCAP_VEP_out_VCF %>% filter(map_lgl(str_remove(HGVSc, "^NM_.*:"), ~ . %in% VEP_not_found))


VEP_not_found_manual <- tribble(
  ~VEP_in, ~CHROM, ~HGVSp, ~POS, ~SYMBOL, ~REF, ~ALT,
  "NM_000546.4:c.473G>A", "17", "p.Arg158His", 7675139, "TP53", "G", "A",
  "NM_000546.4:c.641A>G", "17", "p.His214Arg", 7674890, "TP53", "A", "G",
  
  # by finding the corresponding variant in our Seq panel after looking it up here:
  # https://mutalyzer.nl/position-converter?assembly_name_or_alias=GRCh38&description=NM_001754.4%3Ac.951_952delTT
  # NC_000021.9:g.34799316_34799317delAA => VCF format annotates a DEL starting at the previous base => ...315, so it fits
  "NM_001754.4:c.951_952delTT", "21", "", 34799315, "RUNX1", "GAA", "G",
  "NM_001754.4:c.601C>T", "21", "p.Arg201Ter", 34859486, "RUNX1", "C", "T",
  "NM_001754.4:c.485G>A", "21", "p.Arg162Lys", 34880580, "RUNX1", "G", "A",
  
  # https://mutalyzer.nl/position-converter?assembly_name_or_alias=GRCh38&description=NM_001754.4%3Ac.274A%3EG
  "NM_001754.4:c.274A>G", "21", "", 34886920, "RUNX1", "A", "G",
  
  "NM_001754.4:c.790C>T", "21", "", 34834425, "RUNX1", "C", "T",
  "NM_001754.4:c.620G>A", "21", "p.Arg207Gln", 34834595, "RUNX1", "G", "A",
  
  # https://mutalyzer.nl/position-converter?assembly_name_or_alias=GRCh38&description=NM_001754.4%3Ac.229_248del
  "NM_001754.4:c.229_248del", "21", "", 34886945, "RUNX1", "GGCCAGCACCTCCACCATGCT", "G",
  "NM_012433.2:c.1986C>A", "2", "p.His662Gln", 197402647, "SF3B1", "C", "A",
  
  # 
  "NM_012433.2:c.2225G>A", "2", "", 197401887, "SF3B1", "G", "A") %>%
  
  mutate(HGVSc = VEP_in,
         HGVSc.base = gsub("(NM_\\d*)\\.\\d(:.*)", "\\1\\2", HGVSc),
         VEP_in.base = gsub("(NM_\\d*)\\.\\d(:.*)", "\\1\\2", HGVSc))


D_REDCAP_VEP <- D_REDCAP_VEP_out_VCF %>% full_join(VEP_not_found_manual)
D_REDCAP_VEP_patients <- D_REDCAP_VEP %>% inner_join(D_REDCAP, by = "VEP_in.base") %>%
  mutate(chrom = CHROM, start = POS, ref = REF, alt = ALT, HGVSc = HGVSc.x, HGVSp = HGVSp.x, .keep = "unused") %>%
  dplyr::select(gene, chrom, start, ref, alt, HGVSc, HGVSp, patient, vaf, Consequence, IMPACT, BIOTYPE, EXON, INTRON, Codons,
                STRAND, GIVEN_REF, USED_REF, CLIN_SIG, ID_DBSNP, ID_COSMIC) %>%
  mutate(source = "RedCap") %>%
  group_by(chrom, start) %>%
  mutate(DS_recurrence = length(patient)) %>%
  ungroup


D_REDCAP_VEP_patients %>% group_by(patient, HGVSc.base) %>% mutate(n = length(patient)) %>% arrange(desc(n)) %>% View
