. definitions.sh
. functions_bam.sh
. functions_vcf.sh

#
 ############################## MAIN #############################
#

# Input Parameters
SAMPLE="$1"

FQ="$MAIN/fq/${SAMPLE}_R1.fq.gz,$MAIN/fq/${SAMPLE}_R2.fq.gz"
BAM_MAP="$BATCH/lock/aln/${SAMPLE}.bam"  # sorted BAM file
BAM_DUP="$BATCH/lock/dup/${SAMPLE}_dup.bam"
BAM_BQSR="$BATCH/lock/bqsr/${SAMPLE}_dup_bqsr.bam"

OUT_ISM="$BATCH/lock/ism/${SAMPLE}_ism.out"
OUT_HSM="$BATCH/lock/hsm/${SAMPLE}_hsm.txt"
OUT_VCF="${SAMPLE}.vcf"


map_bwa $FQ $BAM_MAP $SAMPLE
mark_dup $BAM_MAP $BAM_DUP
get_ism $BAM_DUP $OUT_ISM
collect_hs_metrics $BAM_DUP $OUT_HSM
run_bqsr $BAM_DUP $BAM_BQSR
##
#
##
## variant calls
run_vardict $BAM_BQSR $RESULTS/vardict $SAMPLE
run_lofreq $BAM_BQSR $RESULTS/lofreq $SAMPLE
run_snver $BAM_BQSR $RESULTS/snver $SAMPLE
###run_haplotypecaller $BAM_BQSR $VARDICT/$SAMPLE

## ITD calling
run_pindel $BAM_BQSR $RESULTS/pindel $OUT_ISM $SAMPLE
###run_scanitd $BAM_BQSR $RESULTS/scanitd $SAMPLE
soft_clip_spikes $BAM_BQSR $RESULTS/softclip $SAMPLE


