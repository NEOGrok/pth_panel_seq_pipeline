# PTH Pipeline

Pipeline for calling, annotating and filtering variants from panel sequencing. There are 2 versions/branches: one running samples based on GNU parallel (branch Tycho) and one to be run using qsub submissions to a cluster (branch Computerome).
It performs the following steps:

- Pre-processing
	- Alignment
	- MarkDup
	- BQSR
	- HS-Metrics
	- Insert Size Metrics
	
	- Variant Calling
		- SNP + INDEL
			Vardict
			Lofreq
			Snver
		- FLT3ITD
			Vardict
			Pindel
			Softclip
		- CNV
			CNACS
	- Annotation
		VEP

- Middle-processing
	- Average coverage extraction from HS-metrics
	- Data extraction for post-processing
	- Variants (SNP, INDEL, FLT3 ITD, CNV)
	- Annotations

- Post-processing
	- R filtering of variants
		Remove DP < 30
		Remove: 1+ NORMAL VAF > 0.2 
		Remove: 2+ NORMAL 0.1 < VAF < 0.2 
		Keep snpEff Impact: HIGH or MODERATE
		Remove NORMAL only

	- Clustering 
		- If there are both NORMAL and PATIENT
		- ?? If #PATIENT >= 50% of patient samples

	- Cluster Filtering
		- Filter clusters with >1 NORMAL
		- Keep clusters that are
			- 4SD or >0.05 appart
		- Filter samples with VAF < 0.02
		- Filter clusters with >= 50% PATIENTS

	- Whitelist
		- A manually curated list of variants that shouldn't be removed despite potentially failing all filters
		- Justification: no filter is perfect... they're rough guideline that have little to do with the inherent biology and mainly focus on statistics

	- Plots
		- QC plots
		- Horizon Reference variants
		- Interactive Variant Plots

	- Output per patient in an expanded MAF format





## Installation

### Bitbucket repo with code and meta data and empty fq folder


	git clone git@bitbucket.org:NEOGrok/pth_panel_seq_pipeline.git




###### install.sh

	# conda env export > panel_seq_pipeline.yaml
	conda env create -f panel_seq_pipeline.yaml
	# Get these into refs/vep_cache/

	VEP_CACHE="../refs/vep_cache"  # since install.sh is run from code/

	#curl -O ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/homo_sapiens_vep_101_GRCh38.tar.gz
curl -o $VEP_CACHE/homo_sapiens_vep_refseq_101_GRCh38.tar.gz ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/homo_sapiens_vep_refseq_101_GRCh38.tar.gz
	#curl -O ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/homo_sapiens_vep_merged_101_GRCh38.tar.gz



### Dependencies

- GNU parallel
- bwa
- sambamba
- bcftools
- snpEff 4.3
- SnpSift 4.3
- ensembl-vep
- vardict-java
- lofreq
- snver
- pindel


R libraries

- tidyverse
- magrittr
- ggiraph
- patchwork
- zoo

(- tidygenomics)





###### Or manually

NB: snpEff + SnpSift must have the same version:

	#conda install -c conda-forge r-tidyverse r-magrittr r-ggiraph r-patchwork r-zoo parallel
	#conda install -c bioconda snpeff=4.3 bioconda snpsift=4.3 bioconda ensembl-vep vardict-java lofreq snver pindel sambamba bcftools bwa
    conda create --prefix ${PWD}/conda/PTH R=4.0 r-tidyverse r-magrittr r-ggiraph r-patchwork r-zoo parallel snpsift=4.3 ensembl-vep=101.0 vardict-java lofreq snver pindel sambamba bcftools bwa picard gatk4 bedtools numpy moreutils tidygenomics

Get these into refs/vep_cache/

curl -O ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/homo_sapiens_vep_101_GRCh38.tar.gz
curl -O ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/homo_sapiens_vep_refseq_101_GRCh38.tar.gz
curl -O ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/homo_sapiens_vep_merged_101_GRCh38.tar.gz





## Command Line
To run the pipeline use

	bash main_loop.sh -m /path/to/main


The fq and meta files can be softlinks.
The paths to the various reference files are set in the definitions.sh

    pth_panel_seq_pipeline/
    	code/
            ...
	
		fq/
            sample01_R1.fq.gz
    		sample01_R2.fq.gz
    		sample02_R1.fq.gz
    		sample02_R2.fq.gz
            
   		meta/
    		panel_probes.bed
    		panel_targets.bed
    		sample_info.tsv

        	Focused_myeloid_panel-All_target_segments_covered_by_probes-TE-93310852_hg38_v2_190722165759.bed
            Focused_myeloid_panel-Probe_placement_file-TE-93310852_hg38_v2_190722165707.bed
            HorizonMyeloid_vars_hg38.tsv
            MAF_format.tsv
            ProbePlacement_Schmidt_Myeloid_TE-98545653_hg38.bed
            all_target_segments_covered_by_probes_Schmidt_Myeloid_TE-98545653_hg38.bed
            all_target_segments_covered_by_probes_Schmidt_Myeloid_TE-98545653_hg38_190919225222_updated-to-v2.bed
            goi.tsv
            probes_combined_PTH_Comprehensive_Myeloid_Panel_v2_2_TE-98216079_hg38_with-NPM1-ins-probes.bed
            snpeff_ANN_format.txt
            vep_CSQ_format.txt
			
		refs/genome/ensembl/chr_only/
            Homo_sapiens.GRCh38_chrOnly.chrlen
            Homo_sapiens.GRCh38_chrOnly.dict
            Homo_sapiens.GRCh38_chrOnly.fa
            Homo_sapiens.GRCh38_chrOnly.fa.amb
            Homo_sapiens.GRCh38_chrOnly.fa.ann
            Homo_sapiens.GRCh38_chrOnly.fa.bwt
            Homo_sapiens.GRCh38_chrOnly.fa.fai
            Homo_sapiens.GRCh38_chrOnly.fa.pac
            Homo_sapiens.GRCh38_chrOnly.fa.sa
            Homo_sapiens.GRCh38_chrOnly_chr.fa
            Homo_sapiens.GRCh38_chrOnly_chr.fa.fai
            
		refs/var/
            00-All.vcf.gz
        	00-All.vcf.gz.tbi
    	    Cosmic_all.vcf.gz
    	    Cosmic_all.vcf.gz.tbi
    	    clinvar.vcf.gz
    	    clinvar.vcf.gz.tbi

    	    vep_cache/
    	    	indexed_vep_cache/homo_sapiens_vep_refseq_101_GRCh38.tar.gz

